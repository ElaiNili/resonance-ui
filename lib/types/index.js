import PropTypes from 'prop-types';

export default {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  size: PropTypes.oneOf([
    'size-3xs',
    'size-2xs',
    'size-xs',
    'size-s',
    'size-m',
    'size-l',
    'size-xl',
    'size-2xl',
    'size-3xl',
    'size-4xl',
  ]),
  color: PropTypes.oneOf([
    '',
    'red',
    'orange',
    'yellow',
    'olive',
    'green',
    'teal',
    'blue',
    'violet',
    'purple',
    'pink',
    'brown',
    'gray',
    'black',
    'white',
    'binance',
    'bitfinex',
    'bitstamp',
    'coinbasepro',
    'hitbtc',
    'huobipro',
    'okex',
    'poloniex',
    'kraken',
    'bittrex',
    'ftx',
    'bithumb',
    'btc-stb',
    'alt-stb',
    'alt-btc',
    'alt-alt',
    'stb-stb',
  ]),
  exchange: PropTypes.oneOf([
    '',
    'binance',
    'bitfinex',
    'bitstamp',
    'coinbasepro',
    'hitbtc',
    'huobipro',
    'okex',
    'poloniex',
    'kraken',
    'bittrex',
    'ftx',
    'bithumb',
  ]),
  indexGroup: PropTypes.oneOf([
    '',
    'btc-stb',
    'alt-stb',
    'alt-btc',
    'alt-alt',
    'stb-stb',
  ]),
  textAlign: PropTypes.oneOf([
    'left',
    'right',
    'center',
    'justify',
    'start',
    'end',
    'match-parent',
    'start end',
  ]),
};
