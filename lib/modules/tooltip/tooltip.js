import React, { useState, useEffect, useRef } from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

export default function Tooltip({
  id,
  tooltipRef,
  className,
  style,
  children,
  render,
  onOpen,
  onClose,
  zIndex,
  tooltipPosition,
  isCanHover,
  active,
}) {

  const [isHovered, setIsHovered] = useState(false);
  const tooltip = useRef(null);

  // ============================================================+

  const defaultClasses = [
    'tooltipWrapper',
  ];

  const updatedClasses = clsx(defaultClasses, className);
  // ============================================================+

  const defaultTooltipClasses = [
    'tooltip',

  ];

  const updatedTooltipClasses = clsx(defaultTooltipClasses);

  // ============================================================+

  const defaultTooltipMarginClass = [
    'tooltip-margin',
    ((isHovered && isCanHover) || active) ? 'is-visible' : 'is-hidden',
    // true ? 'is-visible' : 'is-hidden',
    tooltipPosition && `tooltip-position-${tooltipPosition}`,
  ];

  const updatedTooltipMarginClass = clsx(defaultTooltipMarginClass);

  // ============================================================+

  const onMouseEnter = () => {
    setIsHovered(true);
  };

  const onMouseLeave = () => {
    setIsHovered(false);
  };


  useEffect(() => {
    if ((isHovered && isCanHover) || active) {
      onOpen();
    }
    if (!(isHovered && isCanHover) || !active) {
      onClose();
    }

  }, [isHovered, isCanHover, active]);

  // ==============================================

  const tooltipStyle = { zIndex, };

  const tooltipMarginStyle = (tooltip && tooltip.current && tooltipPosition === 'top') ? {
    top: `${-tooltip.current?.clientHeight - 20}px`,
  } : {};

  // ==============================================

  return (
    <div
      className={updatedClasses}
      // className='tooltipWrapper'
      id={id}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      ref={tooltipRef}
      style={style}
    >
      {tooltipPosition === 'top' && (
        <div className={updatedTooltipMarginClass}
          style={{ ...tooltipMarginStyle, ...tooltipStyle }}>
          <div
            className={updatedTooltipClasses}
            ref={tooltip}
          >
            {render}
          </div>
        </div>
      )}
      {children}
      {tooltipPosition === 'bottom' && (
        <div className={updatedTooltipMarginClass}
          style={{ ...tooltipMarginStyle, ...tooltipStyle }}>
          <div
            className={updatedTooltipClasses}
            ref={tooltip}
          >
            {render}
          </div>
        </div>
      )}
    </div>
  );
}

Tooltip.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  tooltipRef: PropTypes.object,
  render: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  zIndex: PropTypes.number,
  tooltipPosition: PropTypes.oneOf(['top', 'bottom']),
  isCanHover: PropTypes.bool,
  active: PropTypes.bool,
};

Tooltip.defaultProps = {
  onOpen: (x) => x,
  onClose: (x) => x,
  zIndex: 3,
  tooltipPosition: 'bottom',
  isCanHover: true,
};
