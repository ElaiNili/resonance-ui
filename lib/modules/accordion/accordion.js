import React from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';

export default function Accordion({
  id,
  children,
  className,
  style,

  fluid,
  filling,
  divided,
  basic,
}) {
  const defaultClasses = [
    'accordion',
    !basic && 'accordion-outlined',
    'accordion-basic',
    fluid && 'accordion-fluid',
    !basic && divided && 'accordion-divided',
    filling && 'accordion-filling',
  ];

  const updatedClasses = clsx(defaultClasses, className);

  return (
    <div className={updatedClasses}
      id={id}
      style={style}>
      {children}
    </div>
  );
}

Accordion.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  fluid: PropTypes.bool,
  filling: PropTypes.bool,
  divided: PropTypes.bool,
  basic: PropTypes.bool,
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
};

Accordion.defaultProps = {
  children: null,
  style: {},
};
