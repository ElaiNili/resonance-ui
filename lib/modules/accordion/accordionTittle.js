import React from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';
import { Icon } from '../../index.js';
import types from '../../types/index.js';

function AccordionTittle({
  children,
  className,
  style,
  id,
  as,

  content,

  active,
  onClick,
  disabled,
  size,
}) {
  const defaultClasses = [
    'accordion-tittle',
    !active && 'accordion-tittle-border-bottom',
    active && 'accordion-tittle-active',
    size && `${size}`
  ];

  const updatedClasses = clsx(defaultClasses, className);

  const styleDefault = { ...style };

  const onClickWrapper = (e) => {
    if (disabled) return;
    onClick(e, { id, active: !active });
  };

  const Tag = `${as}`;

  const icon = active ? 'arrow_drop_up' : 'arrow_drop_down';

  return (
    <div
      className={updatedClasses}
      id={id}
      onClick={onClickWrapper}
      role='none'
      style={styleDefault}
    >
      <Tag>
        {children || content || ''}
      </Tag>

      <Icon
        icon={icon}
        size='size-l'
      />
    </div>
  );
}

export default AccordionTittle;

AccordionTittle.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  content: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
  ]),
  as: PropTypes.oneOf(['div', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6']),
  className: PropTypes.string,
  style: PropTypes.object,
  onClick: PropTypes.func,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  isBorder: PropTypes.bool,
  active: PropTypes.bool,
  disabled: PropTypes.bool,
  size: types.size,
};

AccordionTittle.defaultProps = {
  children: null,
  style: {},
  as: 'div',
  onClick: (e) => e,
  isBorder: true,
  active: false,
  size: 'size-m'
};
