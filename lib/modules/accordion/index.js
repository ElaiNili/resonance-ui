import Accordion from './accordion';
import AccordionTittle from './accordionTittle';
import AccordionContent from './accordionContent';
import './accordion.css';

export { Accordion, AccordionTittle, AccordionContent };
