import React from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';

export default function AccordionContent({
  active,
  children,
  className,
  style,
  id,
}) {
  const defaultClasses = [
    'accordion-content',
    active && 'accordion-content-active'
  ];

  const updatedClasses = clsx(defaultClasses, className);

  return active ? (
    <div className={updatedClasses}
      id={id}
      style={style}>
      {children}
    </div>
  ) : (
    ''
  );
}

AccordionContent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  active: PropTypes.bool,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

AccordionContent.defaultProps = {
  children: null,
  style: {},
  active: false,
};
