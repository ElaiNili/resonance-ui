import React, { useEffect, useCallback, useRef } from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';
import { Dimmer } from '../../index';

import './popup.css';

export default function PopUp({
  onClose,
  active,
  children,
  // modalScroll,
  className,
  style,
  id,
  closeOnDimmerClick,
  loading,
}) {

  const el = useRef(null);

  const closePopUp = (e) => {
    if (typeof onClose !== 'function') return;
    onClose(e, { id, active });
  };

  // =====================================================================================
  const body = document.body;

  if (/* modalScroll */ active) {
    // body.style.overflow = "hidden";
    body.classList.add('body-overflow-hidden');
  }

  if (/* modalScroll */ !active) {
    body.classList.remove('body-overflow-hidden');
  }

  // =====================================================================================

  const defaultClasses = ['popup', active && 'is-visible'];

  const updatedClasses = clsx(defaultClasses, className);

  // =====================================================================================
  const modalDefaultClasses = [
    'popup-container',
    'modal-size',
    // modalScroll && '',
  ];

  const updatedModalClasses = clsx(modalDefaultClasses, className);

  // =====================================================================================
  const overlay = {
    onClick: closeOnDimmerClick ? (e) => closePopUp(e) : (e) => e,
  };
  // =====================================================================================

  const onEscapePress = (e) => {
    if (e.key === 'Escape') {
      if (typeof onClose !== 'function') return;
      onClose();
    }
  };

  useEffect(() => {
    if (!active) return;
    if (!closeOnDimmerClick) return;
    if (!el && !el.current) return;

    el.current.focus();
    el.current.addEventListener('keydown', onEscapePress, false);

    return () => {
      el.current?.removeEventListener('keydown', onEscapePress, false);
    };

  }, [active, closeOnDimmerClick]);



  return (
    <div className={updatedClasses}
      id='popup'
      {...overlay}>
      <div
        className={updatedModalClasses}
        id={id}
        onClick={(e) => e.stopPropagation()}
        style={style}
      >
        {active && (
          <Dimmer active={loading}
            className='popup-dimmer'

          >
            {children}
            <button ref={el}
              style={{ opacity: 0, width: '0px', height: '0px', position: 'absolute', pointerEvents: 'none', top: '0', left: '0', }} />
          </Dimmer>
        )}
      </div>
    </div>
  );
}

PopUp.propTypes = {
  onClose: PropTypes.func,
  active: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  closeOnDimmerClick: PropTypes.bool,
  loading: PropTypes.bool,

};

PopUp.defaultProps = {
  onClose: (x) => x,
  active: false,
  children: null,
  className: '',
  style: {},
  id: '',
  closeOnDimmerClick: true,
};
