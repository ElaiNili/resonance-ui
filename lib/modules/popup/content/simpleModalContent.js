import React from 'react';

import PropTypes from 'prop-types';

import { Button, Block, Flexbox } from '../../../../lib/index';

import { Remarkable } from 'remarkable';

const markDown = new Remarkable({ breaks: true });

function SimpleModalContent({ header, message, buttons, onClick }) {
  const isHeader = header.length > 0;
  const isMessage = message.length > 0;
  const isButtons = buttons.length > 0;

  // =================================

  const headerBlock = (
    <div
      dangerouslySetInnerHTML={{
        __html: markDown.render(header),
      }}
    ></div>
  );

  // =================================

  const messageBlock = (
    <div
      dangerouslySetInnerHTML={{
        __html: markDown.render(message),
      }}
    ></div>
  );

  // =================================

  const buttonsBlock = buttons.map((btn) => (
    <Button {...btn}
      className='modal-buttons'
      key={btn.id}
      onClick={onClick} />
  ));

  // =================================

  return (
    <Block basic
      borderless
      className='modal-block'>
      {isHeader && <Block borderless>{headerBlock}</Block>}

      {isMessage && <Block borderless>{messageBlock}</Block>}

      {isButtons && (
        <Block borderless>
          <Flexbox center
            stretch>
            {buttonsBlock}
          </Flexbox>
        </Block>
      )}
    </Block>
  );
}

SimpleModalContent.propTypes = {
  header: PropTypes.string,
  message: PropTypes.string,
  buttons: PropTypes.array,
  onClick: PropTypes.func,
};

SimpleModalContent.defaultProps = {
  header: '',
  message: '',
  buttons: [],
  onClick: (x) => x,
};

export default SimpleModalContent;
