import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import { Heading, Flexbox, Icon, Button } from '../../index.js';

export default function ErrorMessage({ error, id, className, style }) {

  const onClick = (e, { href }) => window.open(href, '_blank');

  const updatedClass = clsx(
    'errorBoundary',
    className,
  );

  const messagePath = 'https://t.me/rinvestfund';

  return (
    <div className={updatedClass}
      id={id}
      style={style}>
      <Flexbox center
        middle>
        <Icon icon='error'
          size='size-4xl'
          style={{ marginRight: '8px' }}/>

        <Heading as='h3'
          style={{ margin: 0 }}>
            Чтото прошло не так.
        </Heading>
      </Flexbox>

      <Heading align='center'
        as='h5'>
        {/* eslint-disable-next-line max-len */}
            С этой страницей что-то пошло не так. Сделай скрин этой ошибки и отправь нам в Telegram.
      </Heading>

      <Button content={'Send message'}
        filling
        href={messagePath}
        onClick={onClick}/>

      <Heading as='p'>
        ErrorMessage: {error.toString()}
      </Heading>
    </div>
  );
}

ErrorMessage.propTypes = {
  error: PropTypes.any,
  id: PropTypes.any,
  className: PropTypes.any,
  style: PropTypes.any,
};

ErrorMessage.defaultProps = {
  error: null,
  id: null,
  className: null,
  style: null,
};
