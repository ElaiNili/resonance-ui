import React, { useState, useEffect } from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import types from '../../types';

import { Button, Loading } from '../../index';

export default function Dimmer({
  id,
  children,
  className,
  style,

  as,
  zIndex,
  size,
  blur,
  opacity,
  icon,
  text,
  textColor,
  active,
  isLoading,
  error,
  onError,
  fitContent,
}) {
  const [isActive, setIsActive] = useState(false);
  const [iconTag, setIconTag] = useState([]);
  const [errorComponent, setErrorComponent] = useState([]);

  const [dimmerComponent, setDimmerComponent] = useState([]);

  const defaultClasses = [
    'dimmer',
    fitContent && 'dimmer-fit-content',
  ];
  const updatedClass = clsx(defaultClasses, className);
  const styleDefault = { ...style };

  const defaultChildrenWrapperClasses = [
    'dimmer-content',
    blur && !opacity && 'blur',
  ];
  const updatedChildrenWrapperClasses = clsx(defaultChildrenWrapperClasses);

  const TextTag = `${as}`;

  useEffect(() => {
    if (icon !== '' || error !== null) {
      const errorText =
        // eslint-disable-next-line no-nested-ternary
        error !== null
          // eslint-disable-next-line no-prototype-builtins
          ? error.hasOwnProperty('e')
            ? error.e.toString().toLowerCase()
            : error.toString().toLowerCase()
          : '';

      const errorIconBuilder = (txt) => {
        if (txt.includes('no data')) return 'report_problem';
        if (txt.includes('timeout reached')) return 'refresh';
        if (txt.includes('request timeout')) return 'refresh';
        return 'question';
      };
      const errorIcon = errorText !== '' ? errorIconBuilder(errorText) : icon;
      // setIconTag(<Icon icon={errorIcon}/>);
      setIconTag(
        <Button
          basic
          icon={errorIcon}
          id={errorText}
          onClick={onError}
          size={size}
          style={{ opacity: 1 }}
        />
      );
    }
  }, [icon, onError, error]);

  useEffect(() => {
    const activeStatus = error ? false : active;
    setIsActive(activeStatus);
  }, [active, error]);

  useEffect(() => {
    if (error) {
      const errorMsg =
        // eslint-disable-next-line no-nested-ternary
        error !== null
          // eslint-disable-next-line no-prototype-builtins
          ? error.hasOwnProperty('e')
            ? error.e.toString()
            : error.toString()
          : '';

      setErrorComponent(
        <div className={updatedClass}
          style={styleDefault}>
          <div className='dimmer-child'
            style={{ zIndex }}>
            <div>{iconTag}</div>

            <TextTag className='dimmer-text'
              style={{ color: `${textColor}` }}>
              {errorMsg}
            </TextTag>
          </div>

          <div className={updatedChildrenWrapperClasses}
            style={{ opacity }}>
            {children}
          </div>
        </div>
      );
    }
  }, [error, iconTag]);

  useEffect(() => {
    if (isActive === false) return;
    setDimmerComponent(
      <div className={updatedClass}
        id={id}
        style={styleDefault}>
        <div className='dimmer-child'
          style={{  zIndex }}>
          {!isLoading && <div>{iconTag}</div>}

          {isLoading && <Loading />}

          {text && (
            <TextTag className='dimmer-text'
              style={{ color: `${textColor}` }}>
              {text}
            </TextTag>
          )}
        </div>

        <div className={updatedChildrenWrapperClasses}
          style={{ opacity }}>{children}</div>
      </div>
    );
  }, [isActive]);

  // eslint-disable-next-line no-nested-ternary
  return error
    ? errorComponent : isActive
      ? dimmerComponent : children;
}

Dimmer.propTypes = {
  active: PropTypes.bool.isRequired,
  as: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4', 'h5']),
  className: PropTypes.string,
  style: PropTypes.object,
  children: types.children,
  text: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
  ]),
  icon: PropTypes.string,
  size: types.size,
  blur: PropTypes.bool,
  textColor: types.color,
  isLoading: PropTypes.bool,
  opacity: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  error: PropTypes.any,
  onError: PropTypes.func,
  zIndex: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  fitContent: PropTypes.bool,
};

Dimmer.defaultProps = {
  active: false,
  as: 'h1',
  className: '',
  style: {},
  children: null,
  text: '',
  icon: '',
  size: 'size-4xl',
  blur: true,
  textColor: null,
  isLoading: true,
  opacity: 0,
  error: null,
  onError: (x) => x,
  zIndex: 12,
  // fitContent: false,
};
