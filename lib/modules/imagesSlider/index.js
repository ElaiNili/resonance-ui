import React, { useMemo } from 'react';

import PropTypes from 'prop-types';

import Block from '../../elements/block/block';
import { Button } from '../../elements/button';
import Flexbox from '../../elements/flexbox/flexbox';
import Heading from '../../elements/heading/heading';


export default function ImagesSlider({
  children,
  index,
  onClickPrev,
  onClickNext,
  heading,
  id,
  style,
  className,
  step,


}) {

  const scrollDistance = index * step;


  const scrollStyle = scrollDistance ? { transform: `translateX(-${scrollDistance}px)` } : {};

  // ==========================================


  // const items = useMemo(() =>  data.map((d, i) =>
  //   (
  //     <React.Fragment
  //       key={i}>
  //       {d.render()}
  //     </React.Fragment>
  //   )
  // ), [data, index]);


  return (
    <Block borderless
      className={className}
      id={id}
      marginless
      paddingless
      style={style} >
      <Flexbox middle
        spaceBetween>
        <Heading>
          {heading}
        </Heading>
        <Flexbox>
          <Button basic
            icon='chevron_left'
            onClick={onClickPrev}/>
          <Button basic
            icon='chevron_right'
            onClick={onClickNext}/>
        </Flexbox>
      </Flexbox>

      <div style={{ overflow: 'hidden' }}>
        <Flexbox className='slider-image-items-wrapper'
          nowrap
          style={scrollStyle}
        >
          {children}
        </Flexbox>
      </div>
    </Block>
  );

}

ImagesSlider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  index: PropTypes.number,
  onClickPrev: PropTypes.func,
  onClickNext: PropTypes.func,
  heading: PropTypes.string,
  step: PropTypes.number,
  className: PropTypes.string,
  style: PropTypes.object,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  // mountVisible: PropTypes.number,
};

ImagesSlider.defaultProps = {
  // data: [
  //   { render: () => <div>0</div> },
  //   { render: () => <div>1</div> },
  //   { render: () => <div>2</div> },
  //   { render: () => <div>3</div> },
  // ],
  index: 0,
  onClickPrev: (x) => x,
  onClickNext: (x) => x,
  heading: 'Header',
  step: 10,
  // mountVisible: 1,
};
