import React from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';

import Block from '../../elements/block/block';
import './tab.css';

function TabPane({ id, children, style, className, borderless }) {
  const defaultClasses = ['tab-pane'];
  const updatedClasses = clsx(defaultClasses, className);

  return (
    <Block
      borderless={borderless}
      className={updatedClasses}
      id={id}
      marginless
      paddingless={borderless}
      style={style}
    >
      {children}
    </Block>
  );
}

TabPane.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  className: PropTypes.string,
  borderless: PropTypes.bool,
  style: PropTypes.object,
};

TabPane.defaultProps = {
  id: '',
  children: null,
  className: '',
  borderless: false,
  style: {},
};

export default TabPane;
