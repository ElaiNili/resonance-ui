import React, { useMemo } from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';

import { Label, Button, Icon, Flexbox } from '../../index.js';

import types from '../../types/index';

function Tab({
  panes,
  className,
  style,
  styleTabGroup,
  activeTab,
  onTabChange,
  defaultTab,
  size,
  fluid,
  onClickClose,
}) {


  const onChange = (e) => {
    onTabChange(e, { id: e.target.id, activeTab: e.target.id });
  };

  // ------------- className --------------

  const defaultClasses = ['tab',

  ];
  const updatedClasses = clsx(defaultClasses, className);

  const defaultTabGroupClasses = [
    'tab-button-group',
    fluid ? 'tab-button-group-fluid' : 'tab-button-group-fit',
  ];

  const updatedTabGroupClasses = clsx(defaultTabGroupClasses);

  const getClassName = (item, id) => {
    const defaultTabButtonClasses = [
      'button',
      'pointer-cursor',
      size && `${size}`,
      item.slim && 'button-slim',
      item.disabled && 'disabled disabled-cursor',
      item.center && 'button-center',
      item.left && 'button-left',
      item.right && 'button-right',
      item.spaceBetween && 'button-space-between',
      item.iconPosition && 'button-icon-position-right',


      item.color !== 'black' && item.color && `${item.color}`,
      !item.active
          && !item.color
          && !item.disabled
          && !item.positive
          && !item.negative
          && 'button-outlined',
      (id === activeTab || item.active) && !item.color && 'button-outlined-active',
      !item.active
          && !item.disabled
          && item.color
          && `bg-transparent border-color-button-outlined-${item.color} button-outline-border bg-hover-half-${item.color}`,
      (id === activeTab || item.active)
          && item.color
          && ` border-color-button-outlined-${item.color}  button-outline-border bg-transparency-${item.color}`,

      item.positive
          && 'bg-transparent border-color-button-outlined-green  bg-hover-half-green green',
      item.negative && 'bg-transparent border-color-button-outlined-red  bg-hover-half-red red',

      item.disabled && !item.active && 'bg-transparent button-outlined-disabled',
      item.disabled && item.active && !item.color && 'button-outlined-disabled-active',
      item.disabled
          && item.active
          && item.color !== ''
          && `button-outlined-disabled-active-color basic-button-active-disabled-bg-${item.color} disabled-text-color cursor-default-button`,

      item.loading && !item.color && 'button-outlined-disabled-active',
      item.loading
          && item.color !== ''
      && `button-outlined-disabled-active-color basic-button-active-disabled-bg-${item.color} disabled-text-color cursor-default-button`,
      item.close && 'button-tabs-close'
    ];
    const updatedTabButtonClasses = clsx(defaultTabButtonClasses, 'button-tabs');
    return updatedTabButtonClasses;
  };


  // ------------- return --------------

  const currentTab = useMemo(() => {
    if (panes.length === 0) return;
    const panesArr = panes.map((tab) => ({ ...tab }));
    const item = panesArr
      .find((item) => item.id === activeTab || defaultTab);
    const tab = item ? item.render() : panesArr[0].render();

    return tab;
  }, [panes, activeTab]);

  const isFluid = fluid ? { width: 100 / panes?.length + '%',  } : {};

  const groupInside = useMemo(() => panes.map((item, i) => (
    <div className={getClassName(item.menuItem, item.id)}
      id={item.id}
      key={i}
      onClick={(e) => onTabChange(e, { id: item.id, activeTab: item.id })}
      // spaceBetween={close || item.menuItem.spaceBetween}
      style={isFluid}
    >
      {typeof item.menuItem === 'string' && item.menuItem}

      {typeof item.menuItem.content === 'string'
          && item.menuItem.content}

      {item.menuItem.label && (
        <Label className='tab-label'
          color={item.menuItem?.color}
          size={item.menuItem?.size}
        >
          {item.menuItem.label}
        </Label>
      )}
      {item.menuItem.icon && (
        <Icon className='tab-icon'
          icon={item.menuItem.icon}
          size={size} />
      )}
      {item.menuItem.close && (
        <Button basic
          circle
          className='tab-button-close'
          icon='close'
          id={item.id}
          onClick={onClickClose}
          size='size-xs'
        />
      )}
    </div>
  )), [panes, activeTab]);


  return (
    <div
      className={updatedClasses}
      style={style}>
      <Flexbox className={updatedTabGroupClasses}
        style={styleTabGroup}>
        {groupInside}
      </Flexbox>
      <div>{currentTab}</div>
    </div>
  );
}

Tab.propTypes = {
  panes: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    menuItem: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    render: PropTypes.func.isRequired,
  })),
  className: PropTypes.string,
  style: PropTypes.object,
  styleTabGroup: PropTypes.object,
  activeTab: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onTabChange: PropTypes.func,
  defaultTab: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  size: types.size,
  fluid: PropTypes.bool,
  onClickClose: PropTypes.func,
};

Tab.defaultProps = {
  panes: [{
    id: '',
    menuItem: {},
    render: (x) => x,
  }],
  className: '',
  style: {},
  onTabChange: (x) => x,
  defaultTab: 0,
  size: 'size-xs',
  fluid: false,
  onClickClose: (x) => x,
};

export default Tab;
