import React, { useEffect, useRef, useState, useMemo } from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import { Button } from '../../elements/button';
import types from '../../types';
import DropdownItem from './dropdownItem';

export default function Dropdown({
  id,
  className,
  style,
  children,

  items,
  content,
  label,
  interactiveContent,
  autoClose,
  outSideClose,
  size,
  position,
  itemsRow,
  nested,
  borderless,

  classNameContent,
  styleContent,
  slim,
  fluid,
  fluidContent,
  basic,
  filling,
  outlined,
  buttonLeft,
  buttonRight,
  spaceBetween,
  disabled,

  slimWidth,

  iconOpen,
  iconClose,
  icon,
  color,

}) {
  const [isOpen, setIsOpen] = useState(false);
  const [currentContent, setCurrentContent] = useState(content);

  const dropdownItems = useMemo(() => {
    if (children !== undefined) {
      return children;
    }
    if (items.length !== 0) {
      return (
        <React.Fragment>
          {items.map((el) => (
            <DropdownItem item={el}
              key={el.id}
              onClick={el.onClick} />
          ))}
        </React.Fragment>
      );
    } else return [];
  }, [children, items]);

  const dropdownRef = useRef();
  const dropHandleOutside = (e) => {
    if (!dropdownRef.current?.contains(e.target)) {
      setIsOpen(false);
    }
  };

  useEffect(() => {
    if (outSideClose) {
      document.addEventListener('mousedown', dropHandleOutside);
    }
    return () => document.removeEventListener('mousedown', dropHandleOutside);
  }, [outSideClose]);

  const changeContent = (e) => {
    setCurrentContent(e.target.innerText);
    if (autoClose) {
      setIsOpen(false);
    }
  };

  const defaultClasses = [
    'dropdown',
    fluid && 'dropdown-fluid',
    size && `${size}`,
  ];

  const defaultClassesContent = [
    'dropdown-content',
    slimWidth && 'dropdown-content-slim-width',
    interactiveContent && 'dropdown-content-interactiveContent',
    size && `${size}`,
    position && `dropdown-content-${position}`,
    itemsRow && 'dropdown-content-itemsRow',
    nested && 'dropdown-content-nested',
    !borderless && 'dropdown-content-border',
    fluidContent && 'dropdown-content-fluid'
  ];

  const updatedClasses = clsx(defaultClasses, className);
  const updatedClassesContent = clsx(defaultClassesContent, classNameContent);

  const labelClasses = ['dropdown-label', size && `second-${size}`];

  const updatedLabelClasses = clsx(labelClasses);

  const iconRotate = (() => {
    if (
      iconOpen !== 'arrow_drop_down'
      || iconClose !== 'arrow_drop_down'
      || icon !== ''
    ) {
      return;
    }
    switch (position) {
      case 'top':
        return isOpen ? '0' : '180';
      case 'left':
        return isOpen ? '270' : '90';
      case 'right':
        return isOpen ? '90' : '270';
      default:
        return isOpen ? '180' : '0';
    }
  })();

  const curIcon = icon === '' ? (isOpen ? iconClose : iconOpen) : icon;
  const isOutlined = !basic && !filling;

  const isBasic = basic && !filling && !outlined;

  const isFilling = filling && !basic && !outlined;

  const onHandleClick = () => {
    setIsOpen((s) => !s);
  };

  const buttonStyle = {
    cursor: disabled ? 'not-allowed' : 'pointer',
  };

  return (
    <div className={updatedClasses}
      key={id}
      ref={dropdownRef}
      style={style}>
      {label !== '' && <div className={updatedLabelClasses}>{label}</div>}

      <Button
        active={isOpen}
        basic={isBasic}
        color={color}
        disabled={disabled}
        filling={isFilling}
        fluid={fluid}
        icon={curIcon}
        iconPosition='right'
        iconRotate={iconRotate}
        left={buttonLeft}
        onClick={onHandleClick}
        outlined={outlined || isOutlined}
        right={buttonRight}
        size={size}
        slim={slim}
        spaceBetween={spaceBetween}
        style={buttonStyle}
      >
        {interactiveContent ? currentContent : content}
      </Button>

      {isOpen && (
        <div
          className={updatedClassesContent}
          onClick={changeContent}
          style={styleContent}
        >
          {dropdownItems}
        </div>
      )}
    </div>
  );
}

Dropdown.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
  style: PropTypes.object,
  children: types.children,
  items: PropTypes.array,
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  interactiveContent: PropTypes.bool,
  autoClose: PropTypes.bool,
  outSideClose: PropTypes.bool,
  size: types.size,
  position: PropTypes.oneOf(['left', 'right', 'top', 'bottom']),
  itemsRow: PropTypes.bool,
  nested: PropTypes.bool,
  classNameContent: PropTypes.string,
  styleContent: PropTypes.object,
  slim: PropTypes.bool,
  fluid: PropTypes.bool,
  fluidContent: PropTypes.bool,
  basic: PropTypes.bool, // = false,
  filling: PropTypes.bool, // = false,
  outlined: PropTypes.bool, // = false,
  buttonLeft: PropTypes.bool, // = false,
  buttonRight: PropTypes.bool, //= false,
  spaceBetween: PropTypes.bool, //= false,
  disabled: PropTypes.bool, //= false,
  iconOpen: PropTypes.string,
  iconClose: PropTypes.string,
  icon: PropTypes.string,
  color: types.color, //= '',
  slimWidth: PropTypes.bool,
  borderless: PropTypes.bool,
};

Dropdown.defaultProps = {
  id: '',
  className: '',
  style: {},
  items: [],
  content: '',
  label: '',
  interactiveContent: false,
  autoClose: false,
  outSideClose: true,
  size: 'size-xs',
  itemsRow: false,
  nested: false,
  classNameContent: '',
  styleContent: {},
  slim: false,
  fluid: false,
  // basic: false,
  // filling: false,
  // outlined: false,
  // buttonLeft: false,
  // buttonRight: false,
  // spaceBetween: false,
  // disabled: false,
  iconOpen: 'arrow_drop_down',
  iconClose: 'arrow_drop_down',
  icon: '',
  color: '',
  slimWidth: false,
  borderless: false,
};
