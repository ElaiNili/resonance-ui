import React from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';

export default function DropdownItem({ id, children, item, onClick, styleDropdownItem }) {
  const { content, className } = item;
  const style = { ...styleDropdownItem, ...item?.style };
  const clickHandler = (e) => {
    if (typeof onClick === 'function') {
      onClick(e, { id, item });
    }
  };

  const defaultClasses = ['dropdown-item'];

  const updatedClasses = clsx(defaultClasses, className);

  return (
    <div
      className={updatedClasses}
      key={id || item.id}
      onClick={clickHandler}
      style={style}
    >
      {children && children}

      {item && content}
    </div>
  );
}

DropdownItem.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  onClick: PropTypes.func,
  className: PropTypes.string,
  style: PropTypes.object,
  item: PropTypes.object,
};

DropdownItem.defaultProps = {
  item: { id: '' },
};
