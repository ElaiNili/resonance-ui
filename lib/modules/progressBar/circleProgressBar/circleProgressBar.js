import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import {
  animated,
  useSpring
} from '@react-spring/web';

// import './circleProgressBar.css';
import CustomIcon from '../../../elements/icon/customicons';

export default function CircleProgressBar({
  id,
  className,
  style,
  // children,
  percent,
  duration,
  delay,
  animation,
  icon,
  color,
}) {

  const styles = { ...style };

  const defaultClasses = [
    'circle-progress-bar-wrapper',
    (percent === 0 || !percent) && 'circle-progress-bar-wrapper-def-color',
    percent !== 0 && color && `${color}`,

  ];

  const updatedClasses = clsx(defaultClasses, className);

  // ===================================================

  const defaultClassesProgressBar = [
    'circle-progress-bar',
    color && `circle-progress-bar-${color}`
  ];

  const updatedClassesProgressBar = clsx(defaultClassesProgressBar);

  // =========================================================

  const calculatePercenst = (120 - (120 / 100) * percent);

  const springs = useSpring({
    delay,
    from: {
      strokeDashoffset: 120,
    },
    to: {
      strokeDashoffset: calculatePercenst,
    },
    config: {

      duration,
    },
    reset: animation,
  });

  const content = icon ? (
    <CustomIcon nameIcon={icon}
      size={'size-3xl'} />
  ) : percent !== undefined && (<div>{percent} %</div>);

  return (
    <div className={updatedClasses}
      id={id}
      style={styles}>
      <div
        className={updatedClassesProgressBar}
      >
        <svg className='chart-circle-progress-bar-background'
          height='40'
          viewBox="0 0 40 40"
          width='40'>
          <path d="M19.9999 38.5001C17.5704 38.5001 15.1648 38.0216 12.9203 37.0919C10.6758 36.1622 8.63633 34.7995 6.91845 33.0816C5.20058 31.3638 3.83788 29.3243 2.90817 27.0798C1.97846 24.8353 1.49995 22.4296 1.49995 20.0002C1.49995 17.5707 1.97846 15.1651 2.90817 12.9206C3.83788 10.6761 5.20058 8.63663 6.91846 6.91875C8.63634 5.20087 10.6758 3.83818 12.9203 2.90847C15.1648 1.97876 17.5705 1.50024 19.9999 1.50024C22.4293 1.50024 24.835 1.97876 27.0795 2.90847C29.324 3.83818 31.3635 5.20088 33.0813 6.91876C34.7992 8.63663 36.1619 10.6761 37.0916 12.9206C38.0213 15.1651 38.4998 17.5707 38.4998 20.0002C38.4998 22.4296 38.0213 24.8353 37.0916 27.0798C36.1619 29.3243 34.7992 31.3638 33.0813 33.0816C31.3635 34.7995 29.324 36.1622 27.0795 37.0919C24.835 38.0216 22.4293 38.5001 19.9999 38.5001L19.9999 38.5001Z"
          />
        </svg>
        <animated.svg
          className={'chart-circle-progress-bar'}
          height="40"
          style={springs}
          viewBox="0 0 40 40"
          width="40"
        >
          <path d="M19.9999 38.5001C17.5704 38.5001 15.1648 38.0216 12.9203 37.0919C10.6758 36.1622 8.63633 34.7995 6.91845 33.0816C5.20058 31.3638 3.83788 29.3243 2.90817 27.0798C1.97846 24.8353 1.49995 22.4296 1.49995 20.0002C1.49995 17.5707 1.97846 15.1651 2.90817 12.9206C3.83788 10.6761 5.20058 8.63663 6.91846 6.91875C8.63634 5.20087 10.6758 3.83818 12.9203 2.90847C15.1648 1.97876 17.5705 1.50024 19.9999 1.50024C22.4293 1.50024 24.835 1.97876 27.0795 2.90847C29.324 3.83818 31.3635 5.20088 33.0813 6.91876C34.7992 8.63663 36.1619 10.6761 37.0916 12.9206C38.0213 15.1651 38.4998 17.5707 38.4998 20.0002C38.4998 22.4296 38.0213 24.8353 37.0916 27.0798C36.1619 29.3243 34.7992 31.3638 33.0813 33.0816C31.3635 34.7995 29.324 36.1622 27.0795 37.0919C24.835 38.0216 22.4293 38.5001 19.9999 38.5001L19.9999 38.5001Z" />
        </animated.svg>

      </div>
      <div className='circle-progress-bar-children'>
        {content}
      </div>
    </div>
  );
}

CircleProgressBar.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
  style: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  percent: PropTypes.number,
  duration: PropTypes.number,
  delay: PropTypes.number,
  animation: PropTypes.bool,
  icon: PropTypes.string,
  color: PropTypes.oneOf(['green', 'red', 'yellow']),

};

CircleProgressBar.defaultProps = {
  children: null,
  style: {},
  className: '',
  id: '',
  duration: 500,
  delay: 500,
  animation: false,
  color: 'green',

};
