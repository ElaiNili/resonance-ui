import React, { useState } from 'react';

import useMeasure from 'react-use-measure';
import { useSpring, animated } from '@react-spring/web';


import clsx from 'clsx';
import PropTypes from 'prop-types';
import types from '../../../types';

export default function ProgressBar({
  id,
  className,
  style,
  delay,
  percent,
  color,
  active,
  empty,
  animation,
  // reversed,
  config,

}) {
  // const [open, toggle] = useState(false);
  const [ref, { width }] = useMeasure();

  const colorWidth = (width / 100) * percent;

  const props = useSpring({
    delay,
    width: colorWidth,
    config,
  });

  const animationStyle = useSpring({
    delay,
    width: animation ? colorWidth : 0,
    config,
  });

  // const animationReversed = useSpring({
  //   delay,
  //   width: reversed ? 0 : colorWidth,
  // });

  // const styles = { ...style };
  // ============================================
  const defaultClasses = [
    'progress-bar',
    !empty && active && color && `bg-${color}`,
    // empty && 'empty',
    // active && 'ACTIVE',
  ];

  const updatedClasses = clsx(defaultClasses, className);
  // ============================================

  const defaultAnimatedDivClasses = [
    'fill',
    // empty && 'fill-empty',
    !empty && color && `bg-${color}`,
  ];

  const updatedAnimatedDivClasses = clsx(defaultAnimatedDivClasses);

  // const aniation =

  const animationUpdatedStyle = (empty || active) ? { background: 'transparent' } : animation ? animationStyle : props;


  return (
    <div
      className={updatedClasses}
      id={id}
      ref={ref}
      style={style}
    >
      <animated.div className={updatedAnimatedDivClasses}
        style={animationUpdatedStyle} />
      {/* <animated.div className={styles.content}>{props.width.to((x) => x.toFixed(0))}</animated.div> */}
    </div>
  );
}

ProgressBar.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
  style: PropTypes.object,
  delay: PropTypes.number,
  percent: PropTypes.number,
  color: types.color,
  active: PropTypes.bool,
  empty: PropTypes.bool,
  animation: PropTypes.bool,
  // reversed: PropTypes.bool,
  config: PropTypes.object,
};

ProgressBar.defaultProps = {
  id: '',
  className: '',
  style: {},
  delay: 500,
  percent: 100,
  color: 'blue',
  active: false,
  empty: false,
  animation: false,
  // reversed: false,
  config: {},

};
