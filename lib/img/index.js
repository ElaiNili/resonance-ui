
import binanceLogo from './binance.png';
import bitfinexLogo from './bitfinex.png';

import bitstampLogo from './bitstamp.png';
import bittrexLogo from './bittrex.png';
import bithumbLogo from './bithumb.png';

import coinbaseLogo from './coinbase.png';

import hitbtcLogo from './hitbtc.png';
import huobiLogo from './huobi.png';
import ftxLogo from './ftx.png';

import krakenLogo from './kraken.png';

import okexLogo from './okex.png';
import poloniexLogo from './poloniex.png';


export default {
  binanceLogo, bitfinexLogo,
  bithumbLogo,
  bitstampLogo, bittrexLogo,  coinbaseLogo, ftxLogo,
  hitbtcLogo, huobiLogo, krakenLogo,  okexLogo, poloniexLogo,
};
