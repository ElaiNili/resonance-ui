import React, { useEffect, useRef, useState } from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';


function Slider({
  id,
  className,
  style,
  data,
  onChange,
  labelClassName,
  disabled,
  value,
}) {

  if (data.length === 0) return null;
  if (!value && value !== 0) return null;

  const [rangeData, setRangeData] = useState([]);

  const rangeEl = useRef();

  useEffect(() => {
    const newData = data.map((label, i) => ({
      ...label, value: i * 10
    }));
    setRangeData(newData);
  }, [data]);


  const amount = (data.length - 1);

  const minValue = 0;
  const maxValue = amount * 10;
  const step = 10;

  const curValue = value;


  // ==================================================================

  const defaultClasses = ['range'];

  const updatedClasses = clsx(defaultClasses, className);

  // ==================================================================

  const defaultRangeClasses = [
    'range-input',
    'range-input-margin',
    'range-fluid',
    disabled && 'range-input-disabled',

  ];

  const updatedRangeClasses = clsx(defaultRangeClasses);

  // ==================================================================

  const breakPointDelimiterClasses = (i, label, value, curValue) => {
    const defaultClasses = [
      (i === 0 || i === (data.length - 1))
        ? 'break-point-delimiter-none'
        : label
          ? 'break-point-delimiter break-point-delimiter-label'
          : 'break-point-delimiter break-point-delimiter-without-label',

      curValue > value ? 'break-point-delimiter-gray' : 'break-point-delimiter-blue',
    ];

    return clsx(defaultClasses);

  };
  // ==================================================================

  const breakPointLabelClasses = (label) => {
    const defaultClasses = [
      label ? 'break-point-label' : 'break-point-label-none'
    ];
    return clsx(defaultClasses, labelClassName);
  };

  // ==================================================================

  const getLabel = (value) => {
    const curBreakpoint = rangeData.filter((d) => d.value === Number(value));
    if (curBreakpoint.length === 0) return '';
    return curBreakpoint[0].label;
  };

  // ==================================================================
  // useMemo(() => {
  // }, [value, rangeData]);

  useEffect(() => {
    if (!rangeEl.current) return;
    // if (disabled) return;
    const target = rangeEl.current;
    const value = target.value;
    const min = target.min;
    const max = target.max;
    const backgroundSizeValue = (value - min) * 100 / (max - min);
    target.style.backgroundSize = backgroundSizeValue + '% 100%';
  }, [value, rangeData]);

  // console.count('render');

  const onChangeWrapper = (e) => {
    if (disabled) return;
    const target = e.target;
    const value = target.value;
    const min = target.min;
    const max = target.max;
    const label = getLabel(value);
    const backgroundSizeValue = (value - min) * 100 / (max - min);
    target.style.backgroundSize = backgroundSizeValue + '% 100%';
    // setCurValue(value);
    onChange(e, { id, value, label, });
  };

  // =========================================================

  const items = rangeData.map(({ value, label }, i) => {
    const cls = breakPointDelimiterClasses(i, label, value, curValue);
    return (
      <div className='break-point'
        key={i}>
        <div className={cls} />

        <div
          className={breakPointLabelClasses(label)}
          key={i}
        >
          {label}
        </div>
      </div>
    );
  });

  // =========================================================


  return (
    <div className={updatedClasses}
      style={style}>
      <input className={updatedRangeClasses}
        id={id}
        list={'rangeMarks' + id}
        max={maxValue}
        min={minValue}
        onChange={onChangeWrapper}
        ref={rangeEl}
        step={step}
        type="range"
        value={value}
      />

      <div className={'range-datalist'}
        id={'rangeMarks' + id}>
        {items}
      </div>
    </div>

  );
}

Slider.propTypes = {
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  data: PropTypes.array,
  defaultValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  onChange: PropTypes.func,
  labelClassName: PropTypes.string,
  disabled: PropTypes.bool,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

Slider.defaultProps = {
  id: '',
  className: '',
  style: {},
  data: [],
  onChange: (x) => x,
  // value: 0,
};


export default Slider;
