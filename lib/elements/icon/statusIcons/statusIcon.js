import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import Icon from '../icon';
import types from '../../../types';


export default function StatusIcon({
  id,
  style,
  className,
  color,
  icon,
  size,
}) {


  const defaultWrapperClasses = [
    'status-icon',
    size && `${size}`,
    color && `bg-${color}`,
  ];

  const updatedClasses = clsx(defaultWrapperClasses, className);

  return (
    <div className={updatedClasses}
      id={id}
      style={style} >
      <Icon
        icon={icon} />

    </div>
  );

}

StatusIcon.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
  style: PropTypes.object,
  icon: PropTypes.string,
  size: types.size,
  color: types.color,
};

StatusIcon.defaultProps = {
  id: '',
  className: '',
  style: {},
  icon: '',
  size: 'size-xs',
  color: '',
};
