import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';
import types from '../../types';

// USE THIS https://fonts.google.com/icons
function Icon({
  id, className, style, icon, rotate, size, color, type, mirrorX, mirrorY
}) {

  const commonClasses = [
    type === 'google'
      ? ['icon-el', size && `${size}`, color && `${color}`]
      : [],
    rotate && `icon-${rotate}`,
    mirrorX && 'icon-mirrorX',
    mirrorY && 'icon-mirrorY'
  ];

  const iconTypeClasses =
    type === 'google'
      ? ['material-icons']
      : type === 'fa'
        ? [`fa fa-${icon}`]
        : [];

  const updatedClass = clsx(commonClasses, iconTypeClasses, className);

  const content = type === 'google' ? icon : null;

  return (
    <span className={updatedClass}
      id={id}
      style={style}>
      {content}
    </span>
  );
}

Icon.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
  style: PropTypes.object,
  icon: PropTypes.string,
  rotate: PropTypes.oneOf(['0', '90', '180', '270']),
  size: types.size,
  color: types.color,
  type: PropTypes.oneOf(['google', 'fa']),
  mirrorX: PropTypes.bool,
  mirrorY: PropTypes.bool
};

Icon.defaultProps = {
  id: '',
  className: '',
  style: {},
  icon: '',
  size: 'size-xs',
  color: '',
  type: 'google',
};

export default Icon;
