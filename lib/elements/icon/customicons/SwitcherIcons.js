import React from 'react';
import WrapperIcon from './WrapperIcon';

import AutoMode from './lib/autoMode';
import Bas from './lib/bas';
import Book from './lib/book';
import CandlestickAnalysis from './lib/candlestickAnalysis';
import Casino from './lib/casino';
import Collections from './lib/collections';
import CurrencyExchange from './lib/currencyExchange';
import Econometrics from './lib/econometrics';
import Forum from './lib/forum';
import Ftt from './lib/ftt';
import FundamentalAnalysis from './lib/fundamentalAnalysis';
import Hippo from './lib/hippo';
import Leaderboard from './lib/leaderboard';
import Liquid from './lib/liquid';
import Logo from './lib/logo';
import ManageAccounts from './lib/manageAccounts';
import MarketDelta from './lib/marketDelta';
import MarketFractality from './lib/marketFractality';
import MarketStat from './lib/marketStat';
import Open from './lib/open';
import Path from './lib/path';
import People from './lib/people';
import Person from './lib/person';
import PsychologyOfTheCrowd from './lib/psychologyOfTheCrowd';
import Quiz from './lib/quiz';
import Quote from './lib/quote';
import ResonanceCommunity from './lib/resonanceCommunity';
import Screener from './lib/screener';
import SpeedPrint from './lib/speedPrint';
import Task from './lib/task';
import TechnicalAnalysis from './lib/technicalAnalysis';
import Tool from './lib/tool';
import TradingStrategies from './lib/tradingStrategies';
import Tree from './lib/tree';
import WaveAnalysis from './lib/waveAnalysis';


export default function SwitcherIcons({ nameIcon, size, style }) {
  let icon = null;
  switch (nameIcon) {
    case 'autoMode':
      icon = <AutoMode/>;
      break;
    case 'bas':
      icon = <Bas/>;
      break;
    case 'book':
      icon = <Book/>;
      break;
    case 'candlestickAnalysis':
      icon = <CandlestickAnalysis/>;
      break;
    case 'casino':
      icon = <Casino/>;
      break;
    case 'collections':
      icon = <Collections/>;
      break;
    case 'currencyExchange':
      icon = <CurrencyExchange/>;
      break;
    case 'econometrics':
      icon = <Econometrics/>;
      break;
    case 'forum':
      icon = <Forum/>;
      break;
    case 'ftt':
      icon = <Ftt/>;
      break;
    case 'fundamentalAnalysis':
      icon = <FundamentalAnalysis/>;
      break;
    case 'hippo':
      icon = <Hippo/>;
      break;
    case 'leaderboard':
      icon = <Leaderboard/>;
      break;
    case 'liquid':
      icon = <Liquid/>;
      break;
    case 'logo':
      icon = <Logo/>;
      break;
    case 'manageAccounts':
      icon = <ManageAccounts/>;
      break;
    case 'marketDelta':
      icon = <MarketDelta/>;
      break;
    case 'marketFractality':
      icon = <MarketFractality/>;
      break;
    case 'marketStat':
      icon = <MarketStat/>;
      break;
    case 'open':
      icon = <Open/>;
      break;
    case 'path':
      icon = <Path/>;
      break;
    case 'people':
      icon = <People/>;
      break;
    case 'person':
      icon = <Person/>;
      break;
    case 'psychologyOfTheCrowd':
      icon = <PsychologyOfTheCrowd/>;
      break;
    case 'quiz':
      icon = <Quiz/>;
      break;
    case 'quote':
      icon = <Quote/>;
      break;
    case 'resonanceCommunity':
      icon = <ResonanceCommunity/>;
      break;
    case 'screener':
      icon = <Screener/>;
      break;
    case 'speedPrint':
      icon = <SpeedPrint/>;
      break;
    case 'task':
      icon = <Task/>;
      break;
    case 'technicalAnalysis':
      icon = <TechnicalAnalysis/>;
      break;
    case 'tool':
      icon = <Tool/>;
      break;
    case 'tradingStrategies':
      icon = <TradingStrategies/>;
      break;
    case 'tree':
      icon = <Tree/>;
      break;
    case 'waveAnalysis':
      icon = <WaveAnalysis/>;
      break;

    default:
      return <></>;
      break;
  }
  return (
    <WrapperIcon key={nameIcon}
      size={size}
      style={style}>
      {icon}
    </WrapperIcon>
  );
}
