import React from 'react';
import styles from './WrapperIcon.module.css';

export default function WrapperIcon({ children, size = 'size-m', style }) {
  const propStyle = { ...style };
  return (
    <span className={`${styles.icon} ${styles[size]}`}
      style={propStyle}>
      {children}
    </span>
  );
}
