import React from 'react';

export default function Casino() {
  return (
    <svg fill="currentColor"
      height="50"
      viewBox="0 0 50 50"
      width="50"
      xmlns="http://www.w3.org/2000/svg">
      <path clipRule="evenodd"
        d="M32.7855 17.8507L25 4.64392L17.2145 17.8507H32.7855ZM35.0792 21.7417H14.9208L9.57754 30.8056H40.4225L35.0792 21.7417ZM42.7162 34.6966H7.28379L1 45.3561H49L42.7162 34.6966Z"
        fillRule="evenodd"/>
    </svg>
  );
}
