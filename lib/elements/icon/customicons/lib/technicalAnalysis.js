import React from 'react';

export default function TechnicalAnalysis() {
  return (
    <svg fill="currentColor"
      height="50"
      viewBox="0 0 50 50"
      width="50"
      xmlns="http://www.w3.org/2000/svg">
      <path d="M7.29168 42.4479L3.64584 38.8021L20.5208 21.9271L28.8021 30.2604L32.7083 25.8334L20.5729 13.5938L7.29168 26.875L3.64584 23.2292L20.5208 6.35419L36.0417 22.0834L44.3229 12.8125L47.8125 16.1979L39.4792 25.625L47.7604 34.0104L44.2188 37.5521L36.1458 29.375L28.9583 37.5521L20.5208 29.2188L7.29168 42.4479Z"/>
    </svg>
  );
}
