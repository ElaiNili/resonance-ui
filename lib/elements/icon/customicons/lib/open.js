import React from 'react';

export default function Open() {
  return (
    <svg fill="currentColor"
      height="50"
      viewBox="0 0 50 50"
      width="50"
      xmlns="http://www.w3.org/2000/svg">
      <path d="M29.1667 12.5V43.75H6.25V39.5833H10.4167V6.25H29.1667V8.33333H39.5833V39.5833H43.75V43.75H35.4167V12.5H29.1667ZM20.8333 22.9167V27.0833H25V22.9167H20.8333Z"/>
    </svg>
  );
}
