import React from 'react';

export default function Leaderboard() {
  return (
    <svg fill="currentColor"
      height="50"
      viewBox="0 0 50 50"
      width="50"
      xmlns="http://www.w3.org/2000/svg">
      <path d="M29.2541 2H12.2378V16.5854H29.2541V2Z"/>
      <path d="M37.7622 17.7073H12.2378V32.2927H37.7622V17.7073Z"/>
      <path d="M21.9614 33.4146H12.2378V48H21.9614V33.4146Z"/>
    </svg>
  );
}
