import React from 'react';

export default function ResonanceCommunity() {
  return (
    <svg fill="currentColor"
      height="48"
      viewBox="0 0 48 48"
      width="48"
      xmlns="http://www.w3.org/2000/svg">
      <path d="M1.84998 34.8V4.70001C1.84998 3.04316 3.19312 1.70001 4.84998 1.70001H31.4C33.0568 1.70001 34.4 3.04316 34.4 4.70001V26.2H10.45L1.84998 34.8ZM14.35 37.95C12.6931 37.95 11.35 36.6069 11.35 34.95V29.2H37.4V11.2H43.15C44.8068 11.2 46.15 12.5432 46.15 14.2V46.25L37.85 37.95H14.35Z"/>
    </svg>
  );
}
