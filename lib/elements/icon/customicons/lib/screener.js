import React from 'react';

export default function Screener() {
  return (
    <svg fill="currentColor"
      height="48"
      viewBox="0 0 48 48"
      width="48"
      xmlns="http://www.w3.org/2000/svg">
      <path d="M5 22.55V7.90002C5 6.24317 6.34315 4.90002 8 4.90002H22.6V22.55H5ZM8 43C6.34315 43 5 41.6569 5 40V25.4H22.6V43H8ZM25.45 22.55V4.90002H40.1C41.7569 4.90002 43.1 6.24317 43.1 7.90002V22.55H25.45ZM25.45 43V25.4H43.1V40C43.1 41.6569 41.7569 43 40.1 43H25.45Z"/>
    </svg>
  );
}
