import React from 'react';

export default function CandlestickAnalysis() {
  return (
    <svg fill="currentColor"
      height="50"
      viewBox="0 0 50 50"
      width="50"
      xmlns="http://www.w3.org/2000/svg">
      <path d="M13.6979 42.5521V37.8646H9.53125V12.1354H13.6979V7.44794H18.5938V12.1354H22.7604V37.8646H18.5938V42.5521H13.6979ZM31.4062 42.5521V31.6146H27.2396V16.3021H31.4062V7.44794H36.3021V16.3021H40.4687V31.6146H36.3021V42.5521H31.4062Z"/>
    </svg>
  );
}
