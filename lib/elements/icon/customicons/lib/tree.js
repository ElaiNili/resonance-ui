import React from 'react';

export default function Tree() {
  return (
    <svg fill="currentColor"
      height="50"
      viewBox="0 0 50 50"
      width="50"
      xmlns="http://www.w3.org/2000/svg">
      <path d="M46.3524 5.78284V22.8648H31.4057V16.459H27.1352V33.541H31.4057V27.1352H46.3524V44.2172H31.4057V37.8115H22.8648V16.459H18.5943V22.8648H3.64758V5.78284H18.5943V12.1886H31.4057V5.78284H46.3524Z"/>
    </svg>
  );
}
