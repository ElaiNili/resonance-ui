import React from 'react';

export default function Book() {
  return (
    <svg fill="currentColor"
      height="50"
      viewBox="0 0 50 50"
      width="50"
      xmlns="http://www.w3.org/2000/svg">
      <path d="M37.5 4.16666H12.5C10.2083 4.16666 8.33334 6.04166 8.33334 8.33332V41.6667C8.33334 43.9583 10.2083 45.8333 12.5 45.8333H37.5C39.7917 45.8333 41.6667 43.9583 41.6667 41.6667V8.33332C41.6667 6.04166 39.7917 4.16666 37.5 4.16666ZM12.5 8.33332H22.9167V25L17.7083 21.875L12.5 25V8.33332Z"/>
    </svg>
  );
}
