import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

// import types from '../../types';

function Image({ id,
  style,
  className, src, size, border, round, heightAuto, sizeAuto }) {
  const defaultClasses = [
    'image',

    size && !sizeAuto && `image-${size}`,
    border && 'image-border',
    round && 'image-round',
    heightAuto && 'image-height-auto',
  ];

  const updatedClassName = clsx(defaultClasses, className);

  const defaultStyles = { ...style };

  return (
    <img className={updatedClassName}
      id={id}
      src={src}
      style={defaultStyles} />
  );
}

Image.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  heightAuto: PropTypes.bool,
  style: PropTypes.object, className: PropTypes.string,
  src: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  border: PropTypes.bool,
  round: PropTypes.bool,
  sizeAuto: PropTypes.bool,
};

Image.defaultProps = {
  id: '',
  size: 'size-xs',
  heightAuto: false,
  sizeAuto: false,
};

export default Image;
