import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

export default function ImageAdaptive({
  id, style, className, src,
  sizeAuto, width, height,
  imgRef
}) {
  const defaultWrapClasses = [
    'image-wrap',
  ];

  const defaultClasses = [
    'image',
    sizeAuto && 'image-auto',
  ];

  const updatedWrapClassName = clsx(defaultWrapClasses, className);
  const updatedClassName = clsx(defaultClasses);

  const defaultStyles = { ...style, width, height };

  return (
    <div className={updatedWrapClassName}
      id={id}
      ref={imgRef}
      style={defaultStyles}>
      <img className={updatedClassName}
        id={id}
        src={src} />
    </div>
  );
}

ImageAdaptive.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  style: PropTypes.object, className: PropTypes.string,
  src: PropTypes.string,
  sizeAuto: PropTypes.bool,
  width: PropTypes.string,
  height: PropTypes.string,
  imgRef: PropTypes.shape({ component: PropTypes.instanceOf(React.Component) })
};

ImageAdaptive.defaultProps = {
  src: ''
};


