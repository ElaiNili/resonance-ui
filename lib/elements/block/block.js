import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import types from '../../types';

const Block = ({
  id,
  children,
  center,
  className,
  style,
  textAlign,
  color,
  fluid,
  marginless,
  paddingless,
  borderless,
  fullscreen,
  basic,
  soft,
  onClick,
}) => {
  const defaultClasses = [
    'block-item',
    center && 'block-item-center',
    fullscreen && 'block-item-fullscreen',
    textAlign && `block-text-${textAlign}`,
    color && `block-border-top-${color}`,
    fluid && 'block-width-fluid',
    basic && 'block-basic',
    soft && 'block-soft-square',
    marginless && 'block-no-margin',
    paddingless && 'block-no-padding',
    borderless && 'block-no-border',
  ];

  const updatedClass = clsx(defaultClasses, className);

  const styleDefault = { ...style };

  return (
    <div
      className={updatedClass}
      id={id}
      onClick={onClick}
      style={styleDefault}
    >
      {children}
    </div>
  );
};

Block.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  center: PropTypes.bool,
  className: PropTypes.string,
  style: PropTypes.object,
  textAlign: PropTypes.oneOf([ 'left', 'right', 'center', 'justify']),
  fullscreen: PropTypes.bool,
  color: types.color,
  fluid: PropTypes.bool,
  soft: PropTypes.bool,
  basic: PropTypes.bool,
  marginless: PropTypes.bool,
  paddingless: PropTypes.bool,
  borderless: PropTypes.bool,
  onClick: PropTypes.func,
};

Block.defaultProps = {
  onClick: (e) => e,
};

export default Block;
