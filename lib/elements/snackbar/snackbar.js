import React, { useState, useEffect } from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import { Icon, Button } from '../../index.js';

function Snackbar({
  active,
  onClose,
  className,
  style,
  type,
  message,
  timeout,
}) {
  useEffect(() => {
    if (!timeout) return;
    let closeTime;
    if (active) {
      closeTime = setTimeout(onClose, timeout * 1000);
    }
    return () => {
      clearTimeout(closeTime);
    };
  }, [active, timeout]);

  const onClickClose = () => onClose();

  const defaultClasses = [
    'snackbar',
    !active && 'snackbar-hide',
    type && `type-${type}`,
  ];

  const updatedClasses = clsx(defaultClasses, className);

  const [icon, setIcon] = useState('');
  useEffect(() => {
    switch (type) {
      case 'error':
        setIcon('error_outline');
        break;
      case 'warn':
        setIcon('report_problem');
        break;
      case 'success':
        setIcon('check_circle');
        break;
      case 'info':
        setIcon('info');
        break;
      default:
        setIcon('');
        break;
    }
  }, [type]);

  if (type === undefined || icon === '') return null;

  return (
    <div className={updatedClasses}
      style={style}>
      <Icon color='white'
        icon={icon}
        key={icon}
        size={'size-l'} />

      <p>{message}</p>

      <Button basic
        circle
        color={'white'}
        icon='close'
        onClick={onClickClose}
      />
    </div>
  );
}

Snackbar.propTypes = {
  active: PropTypes.bool,
  onClose: PropTypes.func,
  className: PropTypes.string,
  style: PropTypes.object,
  type: PropTypes.oneOf(['info', 'error', 'warn', 'success']),
  message: PropTypes.string,
  timeout: PropTypes.number,
};

Snackbar.defaultProps = {
  onClose: (x) => x,
  className: '',
  style: {},
  type: '',
  message: '',
  timeout: 5,
};

export default Snackbar;
