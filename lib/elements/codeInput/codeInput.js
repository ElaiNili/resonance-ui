import React, { useState, useRef } from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';
import Dimmer from '../../modules/dimmer/dimmer';

const CodeInput = ({ digits, loading, onDone, style, className, id }) => {
  const [code, setCode] = useState([...Array(digits)].map(() => ''));

  const inputs = useRef([]);

  const processInput = (e, slot) => {
    const num = e.target.value;
    if (/[^0-9]/.test(num)) return;
    const newCode = [...code];
    newCode[slot] = num;
    setCode(newCode);
    if (slot !== digits - 1) {
      inputs.current[slot + 1].focus();
    }
    if (newCode.every((num) => num !== '')) {
      if (typeof onDone === 'function') {
        onDone(newCode.join(''));
      }
    }
  };

  const onKeyUp = (e, slot) => {
    if (e.keyCode === 8 && !code[slot] && slot !== 0) {
      const newCode = [...code];
      newCode[slot - 1] = '';
      setCode(newCode);
      inputs.current[slot - 1].focus();
    }
  };

  const defaultClasses = [
    'code-input-wrap',
  ];

  const updatedClasses = clsx(defaultClasses, className);

  return (
    <Dimmer active={loading}
      opacity={0.5}
      style={{ width: 'fit-content' }}>
      <div className={updatedClasses}
        id={id}
        style={style}>
        {code.map((num, idx) => (
          <input
            autoFocus={!code[0].length && idx === 0}
            className='code-input'
            inputMode='numeric'
            key={idx}
            maxLength={1}
            onChange={(e) => processInput(e, idx)}
            onKeyUp={(e) => onKeyUp(e, idx)}
            readOnly={loading}
            ref={(ref) => inputs.current.push(ref)}
            size={1}
            type='text'
            value={num}
          />
        ))}
      </div>
    </Dimmer>
  );
};

CodeInput.propTypes = {
  onDone: PropTypes.func.isRequired,
  digits: PropTypes.number,
  loading: PropTypes.bool,
  style: PropTypes.object,
  className: PropTypes.string,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number ]),
};
CodeInput.defaultProps = {
  onDone: (x) => x,
  className: '',
  id: '',
  digits: 4,
  loading: false,
};

export default CodeInput;
