import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import types from '../../types';

export default function RadioButton({
  className,
  style,
  disabled,
  label,
  id,
  active,
  onClick,
  size,
  fluid,
  item,
  color,
}) {
  const onClickWrapper = (e) => {
    if (disabled) return;
    onClick(e, {
      disabled,
      label,
      id,
      active,
      onClick,
      size,
      fluid,
      item,
    });
  };

  const defaultClasses = [
    'radio',

    disabled && 'radio-disabled',
    disabled ? 'disabled-cursor' : 'pointer-cursor',
    size && `${size}`,
    fluid && 'radio-fluid',
  ];

  const updatedClasses = clsx(defaultClasses, className);
  // ===================================
  const defaultRadioLabelClasses = [
    'default-radio-label',
    disabled ? 'disabled-cursor' : 'pointer-cursor',
  ];

  const updatedRadioLabelClasses = clsx(defaultRadioLabelClasses);
  // ===================================
  const defaultRadioButtonClasses = [
    'radio-button',
    // color && `${color}`,
    color && `border-${color}`,
    disabled ? 'radio-button-disabled-color' : 'radio-button-color',
    disabled ? 'disabled-cursor' : 'pointer-cursor',
  ];

  const updatedRadioButtonClasses = clsx(defaultRadioButtonClasses);

  // ======================================= Point ===
  // const defaultRadioPointClasses = [
  //   'radio-point',
  //   disabled ? 'disabled-cursor' : 'pointer-cursor',
  // ];

  const defaultRadioPointClasses = [
    'radio-point',
    disabled && active && 'radio-point-disabled',
    active ? 'radio-point-active' : 'radio-point-hidden',
    color && active && `bg-${color}`,
  ];

  const updatedRadioPointClasses = clsx(defaultRadioPointClasses);

  return (
    <div
      className={updatedClasses}
      id={item?.id || id}
      key={item?.id || id}
      onClick={onClickWrapper}
      style={style}
    >
      <input
        checked={active}
        className='input-radio'
        disabled={disabled}
        readOnly
        style={{ border: '' }}
        type='radio'
      />

      <div className={updatedRadioButtonClasses}>
        <div
          className={updatedRadioPointClasses}
        />
      </div>

      <label className={updatedRadioLabelClasses}>{item?.label || label}</label>
    </div>
  );
}

RadioButton.propTypes = {
  disabled: PropTypes.bool,
  className: PropTypes.string,
  style: PropTypes.object,
  onClick: PropTypes.func,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  size: types.size,
  active: PropTypes.bool,
  item: PropTypes.object,
  fluid: PropTypes.bool,
  color: types.color,
};

RadioButton.defaultProps = {
  className: '',
  style: {},
  onClick: (x) => x,
  id: '',
  size: 'size-xs',
  label: '',
};
