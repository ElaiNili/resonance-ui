import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import { Icon } from '../../index.js';
import Loading from '../loading/loading.js';

import types from '../../types';
import LoadingV2 from '../loading/loadingV2.js';

function Input({
  standard,
  basic,
  id,
  className,
  style,
  type,
  value,
  onChange,
  onKeyDown,
  onKeyUp,
  onFocus,
  onBlur,
  placeholder,
  label,
  required,
  disabled,
  pattern, // нету примеров в доке
  icon,
  size,
  fluid,
  autoComplete, // нету примеров в доке
  autoFocus,
  error,
  success,
  helpText,
  min,
  max,
  name,
  iconPosition,
  loading,
  step,
  maxLength,
  onClickIcon,
}) {
  // ==================== Input Main Wrapper ==========================
  const defaultInputMainWrapperClasses = [
    'input-main-wrapper',
    size && `${size}`,
    fluid && 'input-label-fluid',
  ];
  const updatedInputMainWrapperClass = clsx(
    defaultInputMainWrapperClasses,
    className
  );
  // =================== Input Label ============================

  const defaultClasses = [
    'input-label',
    error && 'input-label-error',
    success && 'input-label-success',
  ];

  const updatedClasses = clsx(defaultClasses);

  const defaultStyles = { ...style };

  // ===================== Input Wrapper ==========================
  const defaultInputWrapperClasses = [
    'input-wrapper',
    fluid && 'input-label-fluid',
  ];
  const updatedInputWrapperClasses = clsx(defaultInputWrapperClasses);

  // ==================== Input Element ==========================
  const defaultInputClasses = [
    'input-element',
    !standard && !basic && 'default-input-border',
    standard && 'standard-input-border',
    basic && 'basic-input-border',
    // ====================================
    fluid && 'input-label-fluid',
    // ====================================
    disabled && 'input-border-disabled disabled-cursor',
    disabled && 'disabled-cursor',
    // ====================================
    error && 'input-element-error',
    // ====================================
    success && 'input-element-success',
    // ====================================
    icon && iconPosition === 'left' && 'input-with-icon-left',
    icon && iconPosition === 'right' && 'input-with-icon-right',

    loading && 'input-border-disabled input-loading',

  ];

  const updatedInputClassName = clsx(defaultInputClasses, className);
  const updatedErrorInputMessageClassName = clsx(
    'input-help-text',
    error && 'input-error-message',
    success && 'input-success-message'
    // className
  );

  const props = { id, label, placeholder };

  const onChangeWrapper = (e) => {
    if (typeof onChange !== 'function') return;
    if (disabled) return;
    e.preventDefault();
    onChange(e, { value: e.target.value, ...props });
  };

  const onFocusWrapper = (e) => {
    if (typeof onFocus !== 'function') return;
    if (disabled) return;
    onFocus(e);
  };
  const onKeyDownWrapper = (e) => {
    if (typeof onKeyDown !== 'function') return;
    if (disabled) return;
    onKeyDown(e);
  };
  const onKeyUpWrapper = (e) => {
    if (typeof onKeyUp !== 'function') return;
    if (disabled) return;
    onKeyUp(e);
  };

  const onBlurWrapper = (e) => {
    if (typeof onBlur !== 'function') return;
    if (disabled) return;
    onBlur(e);
  };

  const onClickIconWrapper = (e) => {
    if (typeof onClickIcon !== 'function') return;
    if (disabled) return;
    onClickIcon(e, { id });

  };

  // =====++++++++++++++++======= Icon =========================
  const isIconButton = typeof onClickIcon === 'function';

  const defaultIconStyles = [
    'input-icon',
    isIconButton && 'input-icon-buton',
    iconPosition === 'right' && 'input-icon-right',
  ];

  const updatedIconClasses = clsx(defaultIconStyles);




  // ===========================================================

  const isLabelActive = label !== '' && label !== null && label !== undefined;

  return (
    <div className={updatedInputMainWrapperClass}
      style={defaultStyles}>

      {isLabelActive && (
        <label className={updatedClasses}
          style={defaultStyles}>
          <p className='input-label-text'>
            {label}

            {required ? <span className='label-required-star'>*</span> : ''}
          </p>
        </label>
      )}

      <div className={updatedInputWrapperClasses}>
        {loading && <LoadingV2 className="input-loading-v2" />}
        <input
          autoComplete={autoComplete}
          autoFocus={autoFocus}
          className={updatedInputClassName}
          disabled={disabled || loading}
          max={max}
          maxLength={maxLength}
          min={min}
          name={name}
          onBlur={onBlurWrapper}
          onChange={onChangeWrapper}
          onFocus={onFocusWrapper}
          onKeyDown={onKeyDownWrapper}
          onKeyUp={onKeyUpWrapper}
          pattern={pattern}
          placeholder={placeholder}
          required={required}
          step={step}
          style={defaultStyles}
          type={type}
          value={value}
        />

        {icon && isIconButton ? (
          <button
            className={updatedIconClasses}
            onMouseDown={onClickIconWrapper}
            tabIndex={-1}
            type='button'
          >
            <Icon
              icon={icon}
              size={size} />
          </button>
        ) : (
          <Icon
            className={updatedIconClasses}
            icon={icon}
            size={size} />
        )}

      </div>

      {helpText && (
        <div className={updatedErrorInputMessageClassName}>{helpText}</div>
      )}
    </div>
  );
}

Input.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
  standard: PropTypes.bool,
  basic: PropTypes.bool,
  type: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onChange: PropTypes.func,
  placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  style: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  pattern: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  icon: PropTypes.string,
  size: types.size,
  fluid: PropTypes.bool,
  autoComplete: PropTypes.oneOf(['on', 'off']),
  error: PropTypes.bool,
  success: PropTypes.bool,
  helpText: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  autoFocus: PropTypes.bool,
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  iconPosition: PropTypes.oneOf(['left', 'right']),
  loading: PropTypes.bool,
  min: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  max: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  step: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  maxLength: PropTypes.string,
  onFocus: PropTypes.func,
  onKeyDown: PropTypes.func,
  onKeyUp: PropTypes.func,
  onBlur: PropTypes.func,
  onClickIcon: PropTypes.func,
};

Input.defaultProps = {
  id: '',
  className: '',
  type: 'text',
  value: '',
  onChange: (x) => x,
  placeholder: '',
  style: {},
  label: '',
  size: 'size-xs',
  autoComplete: 'on',
  name: '',
  iconPosition: 'left',
};

export default Input;
