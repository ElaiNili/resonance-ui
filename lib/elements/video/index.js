import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';


export default function Video({
  src,
  id,
  className,
  style,
  size,
  heightAuto,
  muted,
  onLoad,
}) {


  const defaultClasses = [
    'video',
    size && `image-${size}`,
    heightAuto && 'image-height-auto',
  ];

  const updatedClasses = clsx(defaultClasses, className);


  return (
    <video autoPlay
      className={updatedClasses}
      controls
      id={id}
      // poster
      loop="loop"
      muted={muted}
      style={style}>
      <source src={src} />
    </video>
  );
}

Video.propTypes = {
  src: PropTypes.string,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  heightAuto: PropTypes.bool,
  muted: PropTypes.bool,
};

Video.defaultProps = {
  src: '',
  size: 'size-3xl',
  heightAuto: false,
  muted: true,

};
