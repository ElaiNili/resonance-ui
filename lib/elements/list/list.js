import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import types from '../../types';

const ListHeader = ({ as, context, children, className, style, onClick }) => {
  const updatedClass = clsx(className);

  const TagType = `${as}`;

  return (
    <TagType className={updatedClass}
      onClick={onClick}
      style={style}>
      {context || children}
    </TagType>
  );
};

ListHeader.propTypes = {
  as: PropTypes.oneOf(['li', 'dd', 'dt', 'h1', 'h2', 'h3', 'h4', 'h5', 'p']),
  context: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  onClick: PropTypes.func,
};

ListHeader.defaultProps = {
  as: 'h3',
  className: '',
  style: {},
  onClick: (x) => x,
};

function ListItem({
  as,
  id,
  context,
  children,
  className,
  style,
  // isMarkers,
  base,
  secondary,
  isBorder,
  inline,
  isLinkBorder,
  isLinkBgr,
  onClick,
  // markerColor,

  href,
  value,
  noDecoration,
}) {
  const defaultClasses = [
    'list-item',
    // markerColor === '' &&
    base && 'list-item-base',
    // markerColor === '' &&
    secondary && 'list-item-secondary',
    isBorder && 'list-item-border',
    inline && 'list-item-inline',
    noDecoration && 'no-text-decoration',
    href && 'list-item-link',
    isLinkBorder && 'list-item-item-border blue',
    isLinkBgr && 'list-item-item-bgr blue',
  ];
  const updatedClass = clsx(defaultClasses, className);

  const TagType = `${as}`;

  // eslint-disable-next-line prefer-rest-params
  const onClickWrapper = (e) => onClick(e, { id, ...arguments[0] });

  return (
    <TagType
      className={updatedClass}
      onClick={onClickWrapper}
      style={style}
      value={value}
    >
      {/* <span style={{ color: `${markerColor}` }}>{secondary ? "-" : "●"}</span> {context || children} */}

      {context || children}
    </TagType>
  );
}

ListItem.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  as: PropTypes.oneOf(['li', 'dd', 'dt']),
  context: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  // isMarkers: PropTypes.bool,
  base: PropTypes.bool,
  secondary: PropTypes.bool,
  isBorder: PropTypes.bool,
  inline: PropTypes.bool,
  isLinkBorder: PropTypes.bool,
  href: PropTypes.bool,
  noDecoration: PropTypes.bool,
  isLinkBgr: PropTypes.bool,
  onClick: PropTypes.func,
  // markerColor: types.color,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

ListItem.defaultProps = {
  className: '',
  style: {},
  as: 'li',
  href: false,
  onClick: (x) => x,
  // markerColor: '',

  value: '',
};

const List = ({
  as,
  id,
  children,
  className,
  style,

  header,
  marginless,
  paddingless,
  marker,
  position,
  start,
  reversed,
}) => {
  const defaultClasses = [
    'list-default',
    marginless && 'list-default-no-margin',
    paddingless && 'list-default-no-padding',
    marker && `list-${marker}`,
    position && `marker-${position}`,
  ];

  const updatedClass = clsx(defaultClasses, className);

  const TagType = `${as}`;

  return (
    <div id={id}>
      {header && children[0]}

      <TagType
        className={updatedClass}
        reversed={reversed}
        start={start}
        style={style}
      >
        {children}
      </TagType>
    </div>
  );
};

List.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),

  as: PropTypes.oneOf(['ul', 'ol', 'dl']).isRequired,

  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  header: PropTypes.string,
  marginless: PropTypes.bool,
  paddingless: PropTypes.bool,

  marker: PropTypes.oneOf([
    '',
    'none',
    'lower-roman',
    'upper-roman',
    'open',
    'closed',
    'disc',
    'circle',
    'square',
    'decimal',
    'zero-decimal',
  ]),

  position: PropTypes.oneOf(['inside', 'outside']),

  start: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  reversed: PropTypes.bool,
};

List.defaultProps = {
  as: 'ul',
  className: '',
  style: {},
  marker: '',
  position: 'outside',
  start: '',
};

export { List, ListItem, ListHeader };
