import React, { useEffect, useRef } from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import { Button } from '../button/index';
import types from '../../types';

export default function ButtonToTop({
  id,
  className,
  style,
  size,
  onClick,

}) {

  const buttonEl = useRef(null);

  // =================================================

  const defaultClasses = [
    'button-to-top-wrapper'
  ];

  const updatedClasses = clsx(defaultClasses, className);
  // =================================================

  const onClickWrapper = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    // document.body.scrollTop = 0;
    // document.documentElement.scrollTop = 0;
    if (typeof onClick === 'function') {
      onClick();
    }
  };

  useEffect(() => {
    if (!buttonEl && !buttonEl.current) return;

    const scrollFunction = () => {

      if (document.body.scrollTop > 20
        || document.documentElement.scrollTop > 20) {
        buttonEl.current.classList.add('button-to-top-wrapper-is-visible');
      } else {
        buttonEl.current.classList.remove('button-to-top-wrapper-is-visible');
      }
    };

    window.addEventListener('scroll', scrollFunction);

    return () => {
      window.removeEventListener('scroll', scrollFunction);
    };
  }, []);



  return (
    <div className={updatedClasses}
      id={id}
      ref={buttonEl}
      style={style}
    >
      <Button circle
        className='button-to-top'
        filling
        icon='expand_less'
        onClick={onClickWrapper}
        size={size}>

      </Button>
    </div>
  );
}

ButtonToTop.propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  className: PropTypes.string,
  style: PropTypes.object,
  size: types.size,
  onClick: PropTypes.func,
};

ButtonToTop.defaultProps = {
  size: 'size-xl',
};


