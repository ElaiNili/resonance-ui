import React, { useMemo } from 'react';

import { Remarkable } from 'remarkable';

import clsx from 'clsx';
import PropTypes from 'prop-types';
import types from '../../types';


const markDown = new Remarkable({
  html: true,
  xhtmlOut: false,
  breaks: true,
  langPrefix: 'js',
  typographer: true,
});

export default function MDTextBlock({ id, className, style,
  markdown, size, decorativeColor }) {

  const defaultClasses = [
    'markdown',
    size && `${size}`,
    decorativeColor !== '' && `markdown-list-markers-${decorativeColor}`,
  ];

  const updClassNames = clsx(defaultClasses, className);

  const value = useMemo(() => markdown, [markdown]);

  return (
    <div
      className={updClassNames}
      dangerouslySetInnerHTML={{
        __html: markDown.render(value),
      }}
      id={id}
      style={style}
    ></div>
  );
}

MDTextBlock.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
  style: PropTypes.object,
  markdown: PropTypes.string,
  decorativeColor: types.color,
  size: types.size,
};

MDTextBlock.defaultProps = {
  className: '',
  style: {},
  markdown: '',
  decorativeColor: '',
  size: 'size-xs',
};
