import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';
import types from '../../types';

import Icon from '../icon/icon.js';

function Divider(
  { id, style, className, children, content, icon, size, rotate }
) {

  const defaultClasses = [
    'divider',
    size && `${size}`,
  ];


  const updatedClass = clsx(defaultClasses, className);

  return (
    <div className={updatedClass}
      id={id}
      style={style}>
      {icon && (
        <Icon
          icon={icon}
          rotate={rotate}
          size={size}
          style={{ marginRight: '8px' }} />
      )}
      <span className='divider-content' >
        {content || children}
      </span>
    </div>

  );
}

Divider.propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  style: PropTypes.object,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  icon: PropTypes.string,
  size: types.size,
  rotate: PropTypes.oneOf(['0', '90', '180', '270']),
};

Divider.defaultProps = {
  id: '',
  style: {},
  className: '',
  children: null,
};

export default Divider;

