import React from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';

function Loading({ className, style }) {
  const defaultClasses = ['loading-wrapper'];

  const updatedClass = clsx(defaultClasses, className);

  const defaultStyle = {
    ...style,
  };

  return (
    <div className={updatedClass}
      style={defaultStyle}>
      <div className='loading'>
        <div></div>
        <div></div>
        <div>
          <div></div>
        </div>
        <div>
          <div></div>
        </div>
      </div>
    </div>
  );
}

Loading.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
};

Loading.defaultProps = {
  className: '',
  style: {},
};

export default Loading;
