import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import LoadingIcon from './loadingIcon';
import './loadingV2.css';

export default function LoadingV2({ id, className, style }) {

  const defaultClasses = [
    'loading-v2',
  ];

  const updatedClasses = clsx(defaultClasses, className);
  return (
    <div
      className={updatedClasses}
      id={id}
      style={style}>
      <LoadingIcon/>
    </div>
  );
}

LoadingV2.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
  style: PropTypes.object,
};

LoadingV2.defaultProps = {

};
