import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import { Icon } from '../..';

import types from '../../types';
import LoadingV2 from '../loading/loadingV2';
function Button({
  children,

  id,
  style,
  className,
  type,
  buttonRef,

  content,
  onClick,

  icon,
  iconType,
  iconPosition,
  iconRotate,

  fluid,
  circle,
  size,
  slim,
  center,
  left,
  right,

  basic,
  filling,
  outlined,

  disabled,
  active,
  positive,
  negative,
  loading,

  spaceBetween,

  color,
  iconMarginless,
}) {
  const checkButtonTypes = () => {
    const data = [basic, filling, outlined];
    const res = data.filter((d) => d === true);
    const isMoreThen = res.length > 1;
    return isMoreThen;
  };

  if (checkButtonTypes()) {
    console.error('You add more then 1 button type');
  }

  // ======================================

  // common for all buttons
  const commonStyles = [
    'button',
    'pointer-cursor',
    size && `${size}`,
    fluid && 'button-fluid',
    circle && 'button-circle',
    slim && 'button-slim',
    active && 'default-cursor',
    // disabled || loading && 'disabled disabled-cursor',
    disabled && 'disabled disabled-cursor',
    center && 'button-center',
    left && 'button-left',
    right && 'button-right',
    spaceBetween && 'button-space-between',
    iconPosition && 'button-icon-position-right'
  ];

  // default styles with no props
  const defaultStyles =
    !basic && !outlined && !filling
      ? [
        'default-button',
        active && 'default-button-active',
        disabled && 'default-button-disabled',
        loading && 'default-button-disabled',
      ]
      : [];

  // basic button
  const basicStyles = basic
    ? [
      'basic-button',

      // !color && 'basic-color',
      color !== 'black' && color && `${color}`,

      active && !color && 'basic-button-active',

      color && !active && ` transparency-hover-bg-${color}`,
      active && color && `bg-transparency-${color}`,

      positive && 'bg-transparent green transparency-hover-bg-green',
      negative && 'bg-transparent red transparency-hover-bg-red',

      disabled && 'basic-button-disabled',
      active && disabled && !color && 'basic-button-active-disabled',
      active
          && disabled
          && color
          && `basic-button-active-disabled-bg-${color} cursor-default-button`,
      loading && !color && 'basic-button-active-disabled',
      loading
          && color !== ''
          && `basic-button-active-disabled-bg-${color} cursor-default-button`,
    ]
    : [];

  // filling button
  const fillingStyles = filling
    ? [
      !active
          && !color
          && !positive
          && !negative
          && !disabled
          && 'button-filling bg-blue button-text-color-active-white',

      active
          && !color
          && 'button-filling-active box-shadow-active button-text-color-active-white',

      !active
          && !disabled
          && color
          && `bg-${color} bg-hover-half-${color} button-text-color-white`,

      active
          && color
          && `bg-half-${color} box-shadow-active button-text-color-active-white`,

      positive && 'bg-green bg-hover-half-green button-text-color-white',
      negative && 'bg-red bg-hover-half-red button-text-color-white',

      disabled && !active && 'white filling-button-disabled',

      disabled && active && !color && 'filling-button-active-disabled',
      disabled
          && active
          && color !== ''
          && `bg-quater-${color} bg-quater-${color}-hover disabled-text-color cursor-default-button`,
      loading && !color && 'filling-button-active-disabled',
      loading
          && color !== ''
          && `bg-quater-${color} bg-quater-${color}-hover disabled-text-color cursor-default-button`,
    ]
    : [];

  // outlined button
  const outlinedStyles = outlined
    ? [
      color !== 'black' && color && `${color}`,
      !active
          && !color
          && !disabled
          && !positive
          && !negative
          && 'button-outlined',
      active && !color && 'button-outlined-active',
      !active
          && !disabled
          && color
          && `bg-transparent border-color-button-outlined-${color} button-outline-border bg-hover-half-${color}`,
      active
          && color
          && ` border-color-button-outlined-${color}  button-outline-border bg-transparency-${color}`,

      positive
          && 'bg-transparent border-color-button-outlined-green  bg-hover-half-green green',
      negative && 'bg-transparent border-color-button-outlined-red  bg-hover-half-red red',

      disabled && !active && 'bg-transparent button-outlined-disabled',
      disabled && active && !color && 'button-outlined-disabled-active',
      disabled
          && active
          && color !== ''
          && `button-outlined-disabled-active-color basic-button-active-disabled-bg-${color} disabled-text-color cursor-default-button`,

      loading && !color && 'button-outlined-disabled-active',
      loading
          && color !== ''
          && `button-outlined-disabled-active-color basic-button-active-disabled-bg-${color} disabled-text-color cursor-default-button`,
    ]
    : [];

  const updatedClass = clsx(
    commonStyles,
    defaultStyles,
    basicStyles,
    outlinedStyles,
    fillingStyles,
    className
  );

  // ======================================================

  const isButtonWithText = content || children;

  const iconStyles = [
    'icon-button',
    content || children && 'icon-button-left',
    iconMarginless || circle || !isButtonWithText && 'icon-button-center',
    iconPosition  && `icon-button-${iconPosition}`,
  ];

  const updatedIconClass = clsx(iconStyles);

  // ======================================================

  const onClickHandler = (e) => {
    if (disabled) return;
    // eslint-disable-next-line prefer-rest-params
    onClick(e, { id, ...arguments[0] });
  };

  // ======================================================

  return (
    <button
      className={updatedClass}
      disabled={disabled || loading}
      key={id}
      onClick={onClickHandler}
      ref={buttonRef}
      style={style}
      type={type}
    >
      {loading && <LoadingV2/>}

      {icon  && (
        <Icon
          className={updatedIconClass}
          color={color}
          icon={icon}
          onClick={onClickHandler}
          rotate={iconRotate}
          size={size}
          type={iconType}
        />
      )}

      <div style={{ opacity: loading ? 0.2 : 1, pointerEvents: 'none' }}>
        {content || children}
      </div>

    </button>
  );
}

Button.propTypes = {
  children: types.children,

  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  style: PropTypes.object,
  className: PropTypes.string,
  type: PropTypes.string,
  buttonRef: PropTypes.object,

  content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onClick: PropTypes.func,

  icon: PropTypes.string,
  iconType: PropTypes.oneOf(['google', 'fa']),
  iconPosition: PropTypes.oneOf(['left', 'right']),
  iconRotate: PropTypes.oneOf(['0', '90', '180', '270']),

  fluid: PropTypes.bool,
  circle: PropTypes.bool,
  size: types.size,
  slim: PropTypes.bool,
  center: PropTypes.bool,
  left: PropTypes.bool,
  right: PropTypes.bool,
  spaceBetween: PropTypes.bool,

  basic: PropTypes.bool,
  filling: PropTypes.bool,
  outlined: PropTypes.bool,

  disabled: PropTypes.bool,
  active: PropTypes.bool,
  positive: PropTypes.bool,
  negative: PropTypes.bool,
  loading: PropTypes.bool,
  iconMarginless: PropTypes.bool,

  color: types.color,
};

Button.defaultProps = {
  children: null,

  style: {},
  type: 'button',

  content: '',
  onClick: (x) => x,

  icon: '',
  iconType: 'google',

  fluid: false,
  circle: false,
  size: 'size-xs',
  slim: false,
  center: true,
  left: false,
  right: false,
  spaceBetween: false,

  basic: false,
  filling: false,
  outlined: false,

  disabled: false,
  active: false,
  positive: false,
  negative: false,
  loading: false,
  iconMarginless: false,

  color: undefined,
};

export default Button;
