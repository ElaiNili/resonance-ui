import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';
import { Button } from '.';

function ButtonGroup({
  fluid,
  style,
  className,
  children,
  vertical,
  border,
  id,
  curId,
}) {

  if (!children) return null;

  // ================================================= CheckType
  const checkChildrenTypes = () => {
    const isChildrenButton = children.map((d) => d.props)[0];
    const type = isChildrenButton ? 'xml' : 'array';
    return type;
  };

  // ================================================= Fluid
  const isFluid =
  !vertical && fluid ? { width: 100 / children?.length + '%' } : {};

  // ================================================= ClassName
  const defaultClasses = [
    !vertical && 'button-group',
    vertical && 'button-group-horiz flexbox-column',
    !border && 'button-group-without-border',
    fluid && 'fluid',
  ];
  const updatedClass = clsx(defaultClasses, className);

  // ================================================= Style
  const styleDefault = { ...style };

  // ================================================= OnClick

  const onClickCreater = (item, type) => {
    const onClickWrapper = (e, { id, ...other }) => {
      // setCurBtnId(id);
      if (type === 'xml') item?.props?.onClick(e, { id, ...other });
      if (type === 'array' && item.onClick) item.onClick(e, { id, ...other });
    };
    return onClickWrapper;
  };

  switch (checkChildrenTypes()) {
    case 'xml':
      return (
        <div className={updatedClass}
          id={id}
          style={styleDefault}>
          {children.map((d) => {
            const newChild = {
              ...d,
              props: {
                ...d.props,
                onClick: onClickCreater(d, 'xml'),
                active: d?.props.active || d?.props.id === curId,
                disabled: d?.props.disabled || d?.props.id === curId,
                style: { ...isFluid },
              },
            };
            return newChild;
          })}
        </div>
      );
    case 'array':
      return (
        <div className={updatedClass}
          id={id}
          style={styleDefault}>
          {children.map((d) => {
            const isActive = d.active || (curId === d.id);
            const isDisabled = d.disabled || (curId === d.id);
            return (
              <Button
                {...d}
                active={isActive}
                disabled={isDisabled}
                key={d.id}
                onClick={onClickCreater(d, 'array')}
                style={{ ...isFluid }}
              />
            );
          })}
        </div>
      );
  }
}

ButtonGroup.propTypes = {
  fluid: PropTypes.bool,
  style: PropTypes.object,
  className: PropTypes.string,
  vertical: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  border: PropTypes.bool,
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  curId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

ButtonGroup.defaultProps = {
  fluid: false,
  style: {},
  className: '',
  vertical: false,
  border: true,
  id: '',
  curId: '',
};

export default ButtonGroup;
