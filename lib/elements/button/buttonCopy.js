import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import Button from './button';
import Portal from '../../modules/portal/portal';
import Tooltip from '../../modules/tooltip';

export default function ButtonCopy({ text, label }) {

  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    if (isVisible) setTimeout(() => setIsVisible(false), 1500);
  }, [isVisible]);

  const onClickButtonCopy = () => {
    if (isVisible) return;
    navigator.clipboard
      .writeText(text)
      .then(() => setIsVisible(true))
      .catch(() => console.error('not copied'));
  };

  const tooltipRender = (
    <div style={{ padding: '16px' }}>
      {label}
    </div>
  );

  const buttonRef = useRef(null);
  const contentRef = useRef(null);
  const [newStyleC, setNewStyleC] = useState({});

  useEffect(() => {
    if (!buttonRef?.current) return;
    if (!contentRef?.current) return;

    const { left, bottom, width, height } =
      buttonRef.current.getBoundingClientRect();
    const x = left + (width / 2);
    const y = bottom - height;
    setNewStyleC({
      position: 'fixed',
      inset: '0px auto auto 0px',
      transform: `translate(${x}px, ${y}px)`,
    });
  }, [isVisible]);

  return (
    <div>
      <Button basic
        buttonRef={buttonRef}
        color={isVisible ? 'green' : ''}
        icon={isVisible ? 'done' : 'content_copy'}
        onClick={onClickButtonCopy} />
      <Portal>
        {isVisible
          && (
            <Tooltip active={isVisible}
              isCanHover={false}
              render={tooltipRender}
              style={newStyleC}
              tooltipPosition='top'
              tooltipRef={contentRef}
            />
          )}
      </Portal>
    </div>
  );
}

ButtonCopy.propTypes = {
  text: PropTypes.string,
  label: PropTypes.string
};
