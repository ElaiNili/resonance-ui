import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import types from '../../types';

const Heading = ({
  as,
  children,
  className,
  style,
  align,
  isFirstLetterBig,
  isAllLettersBig,
  color,
  onClick,
  id,
}) => {

  const defaultClasses = [
    'heading-default',
    align && `text-${align}`,
    isFirstLetterBig && 'heading-first-letter-big',
    isAllLettersBig && 'heading-all-letters-big',
    color && `${color}`,
  ];

  const styleDefault = { ...style };

  const updatedClass = clsx(defaultClasses, className);

  const Tag = `${as}`;

  return (
    <Tag className={updatedClass}
      id={id}
      onClick={onClick}
      style={styleDefault}>
      {children}
    </Tag>
  );
};

Heading.propTypes = {
  as: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  align: types.textAlign,
  isFirstLetterBig: PropTypes.bool,
  isAllLettersBig: PropTypes.bool,
  color: types.color,
  onClick: PropTypes.func,
};

Heading.defaultProps = {
  as: 'h1',
  className: '',
  style: {},
  align: 'left',
  isFirstLetterBig: false,
  isAllLettersBig: false,
  color: undefined,
  onClick: (x) => x,
};

export default Heading;
