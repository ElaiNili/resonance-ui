import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';


const Line = ({ type = '', style = {}, className }) => {

  const defaultClasses = [
    'vertical-blue-line',
    type + '-line',
  ];
  const updatedClass = clsx(defaultClasses, className);

  return (
    <div className={updatedClass}
      style={style}></div>
  );
};

Line.propTypes = {
  type: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
};

Line.defaultProps = {
  type: '',
  className: '',
  style: {},
};

export default Line;
