import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';
import Icon from '../icon/icon.js';

import types from '../../types/index.js';

function Label({
  children,
  style,
  className,
  id,
  icon,
  iconPosition,
  color,
  size,
  slim,
  circle,
  content,
  as,
  fluid,
}) {

  const defaultClasses = [
    'label-default',
    circle && 'label-circle',
    color && `bg-${color}`,
    size && `${size}`,
    slim && 'label-slim',
    fluid && 'label-fluid',
    iconPosition === 'right' && `label-icon-position-${iconPosition}`
  ];

  const updatedClass = clsx(defaultClasses, className);
  const styleDefault = { ...style };

  const iconStyles = [
    'label-icon',
    content || children && 'icon-label-left',
    iconPosition  && `icon-label-${iconPosition}`,
  ];

  const updatedIconClass = clsx(iconStyles);

  const TagType = `${as}`;

  return (
    <TagType className={updatedClass}
      id={id}
      style={styleDefault}>
      {icon && (
        <Icon className={updatedIconClass}
          icon={icon}
          size={size} />
      )}

      {content || children}
    </TagType>
  );
}

export default Label;

Label.propTypes = {
  as: PropTypes.string,
  style: PropTypes.object,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  fluid: PropTypes.bool,
  slim: PropTypes.bool,
  circle: PropTypes.bool,
  icon: PropTypes.string,
  color: types.color,
  size: types.size,
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  iconPosition: PropTypes.oneOf(['left', 'right']),
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
};

Label.defaultProps = {
  as: 'div',
  style: {},
  className: '',
  children: null,
  color: 'blue',
  size: 'size-xs',
  content: '',
};
