import React, { useEffect, useRef, useState } from 'react';

import PropTypes from 'prop-types';

import rjs from '../../../lib/js/index.js';
const { throttle, screenSize } = rjs;

const { width, height } = screenSize();

function ResizeWrapper({ children, child, parent, fps }) {

  const resizeContainer = useRef(null);

  const [sizes, setSizes] = useState({ width, height });
  const [resizeTimeout] = useState(fps);

  const [childrenSize, setChildrenSize] = useState({ width: [], height: [] });
  const [childOfChildren, setChildOfChildren] = useState([]);
  const [parentSize, setParentSize] = useState({ width: 0, height: 0 });

  const viewPortResizeHandler = () => setSizes({
    width, height
  });

  const domResizeHandle = () => {
    if (resizeContainer.current) {
      if (parent) {
        const prnt = resizeContainer.current.parentElement;
        setParentSize({ width: prnt.clientWidth, height: prnt.clientHeight });
      }
      if (child) {
        if (resizeContainer.current.children.length > 0) {
          const children = Array.from(resizeContainer.current.children);
          setChildrenSize({
            width: children.map((d) => d.clientWidth),
            height: children.map((d) => d.clientHeight)
          });

          const childOfChilds = children.map((ch) => Array.from(ch.children)
            .map((nch) => [nch.clientWidth, nch.clientHeight]));
          setChildOfChildren(childOfChilds);
        }
      }
    }
  };

  // const throttler = fps => (callback, ...data) => {
  //     const timeout = Math.floor(1000 / fps);
  //     let resizeTimeout;
  //     if (!resizeTimeout) {
  //         resizeTimeout = setTimeout(() => {
  //             resizeTimeout = null;
  //             callback(...data);
  //         }, timeout);
  //     };
  // };

  useEffect(() => {
    const fn = throttle(domResizeHandle, resizeTimeout);
    const observer = new ResizeObserver(fn);
    observer.observe(resizeContainer.current);
    return () => observer.disconnect();
  }, [resizeContainer.current, fps]);

  useEffect(() => {
    const fn = throttle(viewPortResizeHandler, resizeTimeout);
    window.addEventListener('resize', fn, false);
    return () => window.removeEventListener('resize', fn);
  }, [fps]);


  return (
    <div className='resize-block-component'
      ref={resizeContainer}>
      {children({
        viewPort: sizes,
        child: childrenSize,
        parent: parentSize, childOfChildren
      })}
    </div>
  );
}

ResizeWrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
    PropTypes.node,
    PropTypes.bool,
    PropTypes.elementType,
  ]),
  child: PropTypes.bool,
  parent: PropTypes.bool,
  fps: PropTypes.number,
};

ResizeWrapper.defaultProps = {
  children: null,
  child: false,
  parent: false,
  fps: 20,
};

export default ResizeWrapper;
