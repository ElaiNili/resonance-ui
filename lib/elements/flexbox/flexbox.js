import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

// https://www.freecodecamp.org/news/an-animated-guide-to-flexbox-d280cf6afc35#.3vx3dhsgh

export default function Flexbox({
  className,
  style,
  children,

  row,
  column,

  nowrap,

  // main axis
  start,
  end,
  center,
  spaceBetween,
  spaceAround,

  // cross axis
  baseline,
  top,
  bottom,
  middle,
  stretch,
}) {
  const defaultClasses = [
    'flexbox',

    row && 'flexbox-row',
    column && 'flexbox-column',
    nowrap && 'flexbox-nowrap',

    start && 'flexbox-content-start',
    end && 'flexbox-content-end',
    center && 'flexbox-content-center',
    spaceBetween && 'flexbox-content-space-between',
    spaceAround && 'flexbox-content-space-around',


    stretch && 'flexbox-align-items-stretch',
    baseline && 'flexbox-align-items-baseline',
    top && 'flexbox-align-items-start',
    bottom && 'flexbox-align-items-end',
    middle && 'flexbox-align-items-center',
  ];

  const updatedClass = clsx(defaultClasses, className);

  return (
    <div className={updatedClass}
      style={style}>
      {children}
    </div>
  );
}

Flexbox.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),

  row: PropTypes.bool,
  column: PropTypes.bool,
  nowrap: PropTypes.bool,

  start: PropTypes.bool,
  end: PropTypes.bool,
  center: PropTypes.bool,
  spaceBetween: PropTypes.bool,
  spaceAround: PropTypes.bool,


  top: PropTypes.bool,
  bottom: PropTypes.bool,
  middle: PropTypes.bool,
  stretch: PropTypes.bool,
  baseline: PropTypes.bool,
};

Flexbox.defaultProps = {
  className: '',
  style: {},

  // row: false,
  // column: false,
  // nowrap: false,

  // start: false,
  // end: false,
  // center: false,
  // spaceBetween: false,
  // spaceAround: false,


  // top: false,
  // bottom: false,
  // middle: false,
  // stretch: false,
  // baseline: false,
};
