import React, { useEffect, useState } from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import { Button } from '../../index';
import types from '../../types';

import ResizeWrapper from '../resizeWrapper';

function Pagination({
  id,
  totalPages,
  activePage,
  onChange,
  size,
  className,
  style,
}) {

  const [buttons, setButtons] = useState([]);
  const [pagiButtons, setPagiButtons] = useState([]);
  const [parentWidth, setParentWidth] = useState(null);


  const onChangeWrapper = (e, { id }) => {
    new Promise((resolve) => {
      if (isNaN(id) === false) {
        resolve(Number(id));
      } else {
        if (id === '+') {
          const res = activePage !== totalPages ? activePage + 1 : totalPages;
          resolve(res);
        }
        if (id === '-') {
          const res = activePage !== 1 ? activePage - 1 : 1;
          resolve(res);
        }
      }
    }).then((page) => {
      onChange(e, { activePage: page, totalPages: Number(totalPages) });
    }).catch(console.error);
  };

  useEffect(() => {
    const pages = [];
    for (let i = 1; i <= totalPages; i++) {
      const offset = i === 1 || totalPages ? 1 : 0;

      if (parentWidth >= 700 && i === 1 && activePage !== 1 && activePage !== 2)
        pages.push(i);

      if (
        parentWidth >= 540
        && activePage === 1
        && (i === activePage + 2 || (parentWidth >= 700 && i === activePage + 3))
      )
        pages.push(i);
      if (activePage === 2 && i === activePage + 2 && parentWidth >= 700) {
        pages.push(i);
      }

      if (parentWidth >= 900) {
        if (activePage === 1 && i === activePage + 3 && (totalPages > 5)) pages.push('...');
        if (activePage === 2 && i === activePage + 2 && (totalPages > 5)) pages.push('...');
        if (activePage === 3 && i === activePage + 2 && (totalPages > 5)) pages.push('...');
        // if(curPage === 4 && i === curPage + (offset + 1)) pages.push('...');

        if (
          3 < activePage
          && activePage <= totalPages - 3
          && i === activePage - (offset + 1)
          && (totalPages > 5)
        )
          pages.push('...');
        if (
          3 < activePage
          && activePage <= totalPages - 3
          && i === activePage + (offset + 1)
          && (totalPages > 5)
        )
          pages.push('...');
      }

      if (parentWidth >= 540) {
        if (i === activePage - offset && activePage !== 1) pages.push(i);
      }

      if (i === activePage) pages.push(i);

      if (parentWidth >= 540) {
        if (i === activePage + offset && activePage !== totalPages) pages.push(i);
      }
      if (parentWidth >= 900) {
        if (activePage === totalPages - 2 && i === activePage - 2 && (totalPages > 5)) pages.push('...');
        if (activePage === totalPages - 1 && i === activePage - 2 && (totalPages > 5)) pages.push('...');
        if (activePage === totalPages && i === activePage - 3 && (totalPages > 5)) pages.push('...');
      }

      // if(parentWidth >= 700 && totalPages === 4 && i === curPage + offset && )

      if (activePage === totalPages - 1 && i === activePage - 2 && parentWidth >= 700) pages.push(i);

      // if (parentWidth >= 700 && (i === curPage + offset) && curPage + offset !== totalPages) pages.push(i);

      if (parentWidth >= 540 && activePage === totalPages && (i === totalPages - 2 || (parentWidth >= 700 && i === totalPages - 3))) pages.push(i);

      if (
        parentWidth >= 700
        && i === totalPages
        && activePage !== totalPages
        && activePage !== totalPages - 1
      )
        pages.push(i);
    }

    const deleteRepeatedNumbers = (arr) => arr.filter((d, index) => {
      d;

      const isNumber = typeof d === 'number';
      if (!isNumber) return d;
      if (isNumber) {
        return arr.indexOf(d) === index;
      }
    });

    setButtons(deleteRepeatedNumbers(pages));
  }, [activePage, totalPages, parentWidth]);

  useEffect(() => {
    const pagesArr = buttons.map((item) => ({
      content: item,
      onClick: onChangeWrapper,
      id: item,
      disabled: item === '...',
      active: item === activePage,
    }));
    const pagiButtonsArr = [];
    // big left arrow
    pagiButtonsArr.push({
      icon: 'keyboard_double_arrow_left',
      onClick: onChangeWrapper,
      id: 1,
      disabled: activePage === 1,
      size,
    });
    // small left arrow
    pagiButtonsArr.push({
      icon: 'keyboard_arrow_left',
      onClick: onChangeWrapper,
      id: '-',
      disabled: activePage === 1,
      size,
    });
    // numbers
    // if (parentWidth >= 540)
    pagiButtonsArr.push(...pagesArr);
    // small right arrow
    pagiButtonsArr.push({
      icon: 'keyboard_arrow_right',
      onClick: onChangeWrapper,
      id: '+',
      disabled: activePage === totalPages,
      size,
    });
    // big right arrow
    pagiButtonsArr.push({
      icon: 'keyboard_double_arrow_right',
      onClick: onChangeWrapper,
      id: totalPages,
      disabled: activePage === totalPages,
      size,
    });
    setPagiButtons(pagiButtonsArr);
  }, [buttons, parentWidth]);

  const defaultClasses = ['pagination'];

  const updatedClasses = clsx(defaultClasses, className);

  const styleDefault = { ...style };

  return (
    <ResizeWrapper child
      parent>
      {(sizes) => {
        useEffect(() => {
          setParentWidth(sizes.parent.width);
        }, [sizes]);

        return (
          <div className={updatedClasses}
            id={id}
            style={styleDefault}>
            {pagiButtons.map((item, iter) => {
              const isOutlined = typeof item.content === 'number';
              return (
                <Button
                  basic={!isOutlined}
                  className={'pagination-buttons'}
                  key={iter}
                  size={size}
                  {...item}
                  outlined={isOutlined}
                />
              );
            })}
          </div>
        );
      }}
    </ResizeWrapper>
  );
}

Pagination.propTypes = {
  totalPages: PropTypes.number.isRequired,
  onChange: PropTypes.func,
  activePage: PropTypes.number.isRequired,
  size: types.size,
  className: PropTypes.string,
  style: PropTypes.object,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

Pagination.defaultProps = {
  totalPages: 10,
  onChange: (x) => x,
  activePage: 1,
  size: 'size-xs',
};

export default Pagination;
