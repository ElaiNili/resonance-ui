import React from 'react';
import partLogo from './img/partLogo.svg';
import dots from './img/dots.svg';
import line from './img/line.svg';
import style from './BackgroundStyle.module.css';

export default function BackgroundStyle() {

  const vwWidth = window.innerWidth;

  return (
    <>
      {vwWidth > 1080 && (
        <img className={style.partLogoImg}
          src={partLogo} />
      )}
      {vwWidth > 850 && (
        <img className={style.dotsLeftImg}
          src={dots} />
      )}
      {vwWidth >= 1500 && (
        <img className={style.dotsRightImg}
          src={dots} />
      )}
      {vwWidth > 800 && (
        <img
          className={vwWidth > 1070 ? style.lineImg : style.lineImgSmall}
          src={line}
        />
      )}
    </>
  );
}
