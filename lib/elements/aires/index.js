import React, { useRef, useEffect } from 'react';
import Typed from 'typed.js';
import PropTypes from 'prop-types';

import logo from './logo/logo.svg';

import { Flexbox, Image, Heading } from '../..';

import './aires.css';
import clsx from 'clsx';

export default function Aires({
  data,
  id,
  style,
  className,
  typeSpeed,
  backSpeed,
  onComplete,
  header,
}) {

  const element = useRef(null);
  const typed = useRef(null);

  // ===========================================

  useEffect(() => {
    const options = {
      strings: data,
      typeSpeed, // 68
      backSpeed, // 0
      onComplete,
    };
    typed.current = new Typed(element.current, options);
    return () => typed.current.destroy();
  }, [data, typeSpeed, backSpeed, onComplete]);

  // ===========================================

  const defaultClasses = [
    'aires'
  ];

  const updatedClasses = clsx(defaultClasses, className);

  const defaultStyles = {
    ...style
  };

  // ===========================================

  return (
    <div className={updatedClasses}
      id={id}
      style={defaultStyles}>
      <Flexbox style={{ alignItems: 'center', marginBottom: '32px' }}>
        <Image sizeAuto
          src={logo} />
        <Heading className='aires-header' >{header}</Heading>
      </Flexbox>
      {/* <div id="typed-strings"
        style={{ display: 'none' }}>Text here</div> */}
      <span
        className=''
        ref={element}
        style={{ whiteSpace: 'pre-wrap', }}
      ></span>
      {/* <span style={{ visibility: 'hidden' }}>|</span> */}
    </div>
  );
}


Aires.propTypes = {
  data: PropTypes.arrayOf(PropTypes.string),
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  style: PropTypes.object,
  className: PropTypes.string,
  typeSpeed: PropTypes.number,
  backSpeed: PropTypes.number,
  onComplete: PropTypes.func,
  header: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

Aires.defaultProps = {
  data: [''],
  typeSpeed: 30,
  // typeSpeed: 20,
  backSpeed: 0,
  onComplete: (self) => console.log('typed info ', self),
  header: 'Айрес'
};
