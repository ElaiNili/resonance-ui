import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import { Icon } from '../../index';

export default function Checkbox({
  onChange,
  checked,
  label,
  disabled,
  className,
  style,
  id,
  size,
  fluid,
  position,
  linkPath,
  linkPart,
  color,
  isTextDecoration,
}) {
  const onChangeWrapper = (e) => {
    if (disabled) return;
    onChange(e, { id, checked, label });
  };

  // ====================================================================================
  const defaultClasses = [
    'checkbox-wrapper',
    disabled && 'checkbox-wrapper-disabled',
    size && `${size}`,
    fluid && 'checkbox-fluid',
    position && `checkbox-wrapper-${position}`,
  ];

  const updatedClassName = clsx(className, defaultClasses);

  // ====================================================================================

  const defaultCheckboxClasses = [
    'checkbox-icon-element',
    color && checked && `bg-${color}`,
    color && `border-${color}`,
    disabled ? 'disabled-cursor' : 'pointer-cursor',
    disabled
      ? checked
        ? 'checkbox-icon-element-disabled-checked'
        : 'checkbox-icon-element-disabled-color'
      : checked
        ? 'checkbox-icon-element-checked'
        : 'checkbox-icon-element-color',
  ];

  const updatedCheckboxClasses = clsx(defaultCheckboxClasses);
  // ====================================================================================

  const isLabelLink = linkPath !== '';

  const isLabelLinkPart = linkPart !== '';

  return (
    <div className={updatedClassName}
      onClick={onChangeWrapper}
      style={style}>
      <input
        checked={checked}
        className='checkbox-input'
        disabled={disabled}
        readOnly
        type='checkbox'
        value={label}
      />

      <div className={updatedCheckboxClasses}>
        {checked && (
          <Icon
            className='icon-checkbox'
            icon='check'
            size={size}
          />
        )}
      </div>

      {isLabelLink ? isLabelLinkPart ? (
        <label className={disabled ? 'disabled-cursor ' : 'pointer-cursor '}>
          <span style={{ marginRight: '4px' }} >{label}</span>
          <a className='checkbox-label-link'
            href={linkPath}
            rel="noreferrer"
            style={{  textDecoration: isTextDecoration ? 'underline' : 'none',  }}
            target={'_blank'}>
            {linkPart}
          </a>
        </label>
      ) : (
        <a className='checkbox-label-link'
          href={linkPath}>
          {label}
        </a>
      ) : label !== '' ? (
        <label className={disabled ? 'disabled-cursor ' : 'pointer-cursor '}>
          {label}
        </label>
      ) : (
        ''
      )}
    </div>
  );
}

Checkbox.propTypes = {
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  className: PropTypes.string,
  style: PropTypes.object,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  checked: PropTypes.bool,
  label: PropTypes.string,
  fluid: PropTypes.bool,
  position: PropTypes.oneOf(['left', 'top', 'right', 'bottom']),
  size: PropTypes.string,
  linkPath: PropTypes.string,
};

Checkbox.defaultProps = {
  onChange: (x) => x,
  disabled: false,
  className: '',
  style: {},
  checked: false,
  fluid: false,
  size: 'size-xs',
  linkPath: '',
  id: '',
  label: '',
};
