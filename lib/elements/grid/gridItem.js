import React from 'react';

import PropTypes from 'prop-types';
export default function GridItem({
  children,
  className,
  style,
  onClick,
}) {

  const onClickWrapper = (e) => onClick(e, {});

  return (
    <div className={className}
      onClick={onClickWrapper}
      style={style}>
      {children}
    </div>
  );
}

GridItem.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  onClick: PropTypes.func,
};
GridItem.defaultProps = {
  children: null,
  className: '',
  style: {},
  onClick: (x) => x,
};
