import React from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';
export default function Grid({
  children,
  className,
  style,
  rows,
}) {

  const defaultClasses = [
    'grid',
  ];

  const rowsBuilder = (row) => {
    const isArr = Array.isArray(row);
    return isArr ? row.join(' ') : `repeat(${rows}, 1fr)`;
  };

  const styles = {
    gridTemplateColumns: rowsBuilder(rows),

    ...style,
  };

  const updatedClass = clsx(defaultClasses, className);

  return (
    <div className={updatedClass}
      style={styles}>
      {children}
    </div>
  );
}

Grid.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  rows: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.array,
  ]),
};

Grid.defaultProps = {
  children: null,
  className: '',
  style: {},
  rows: 2,
};
