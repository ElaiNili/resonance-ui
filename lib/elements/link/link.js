import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';
import types from '../../types';
import Icon from '../icon/icon';

function Link({
  id,
  children,
  className,
  style,
  href,
  content,
  align,
  target,
  size,
  icon,
  color,
  onClick,
}) {

  const onClickWparer = (e) => {
    if (typeof onClick !== 'function')
      return;

    e.preventDefault();
    onClick();
  };

  const defaultClasses = [
    'link-default',
    align && `text-align-${align}`,
    size && `${size}`,
    color && `${color}`,
    color && 'link-color',
    icon && 'link-icon',
    !icon && 'link-underline'
  ];

  const updatedClass = clsx(defaultClasses, className);
  const styleDefault = { ...style };

  return (
    <span className={updatedClass}
      id={id}
      style={styleDefault}>

      <a href={href}
        onClick={onClickWparer}
        target={target}>
        {icon && (
          <Icon icon={icon}
            size={size} />
        )}
        {children || content}
      </a>
    </span>
  );
}

Link.propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  style: PropTypes.object,
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  href: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  align: types.textAlign,
  target: PropTypes.oneOf(['_blank', '_self', '_parent', '_top']),
  size: types.size,
  icon: PropTypes.string,
  color: types.color,
  onClick: PropTypes.func
};

Link.defaultProps = {
  id: '',
  href: '#',
  align: 'left',
  target: '_self',
  size: 'size-xs',
};

export default Link;
