import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

function Container({
  children,
  content,
  className,
  style,
  id,
  border,
  textAlign,
  fluid,
  indent,
  lineHeight,
}) {
  const defaultClasses = [
    'default-container',
    border && 'container-border',
    textAlign && `block-text-${textAlign}`,
    fluid && 'block-width-fluid',
    indent && 'text-indent',
  ];

  const lineHeighthStyle = {
    lineHeight,
  };

  const updatedClass = clsx(defaultClasses, className);
  const styleDefault = { ...style, ...lineHeighthStyle };

  return (
    <div className={updatedClass}
      id={id}
      style={styleDefault}>
      <p>{children || content}</p>
    </div>
  );
}

Container.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
  style: PropTypes.object,
  border: PropTypes.bool,
  textAlign: PropTypes.string,
  fluid: PropTypes.bool,
  indent: PropTypes.bool,
  lineHeight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
};

Container.defaultProps = {
  children: null,
  content: null,
  className: '',
  style: {},
  border: false,
  textAlign: 'left',
  fluid: false,
  indent: false,
  lineHeight: '1.15',
  id: '',
};

export { Container };
