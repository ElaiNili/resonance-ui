import clsx from 'clsx';
import PropTypes from 'prop-types';
import React from 'react';

import types from '../../types/index';

export default function Textarea({
  className,
  placeholder,
  onChange,
  value,
  autoFocus,
  style,
  marginless,
  id,
  fluid,
  size,
  disabled,
  onKeyDown,
  onKeyUp,
  onFocus,
  onBlur,
}) {

  const defaultClasses = [
    'textarea',
    marginless && 'textarea-no-margin',
    fluid && 'textarea-fluid',
    size && `${size}`,
  ];

  const updatedClass = clsx(defaultClasses, className);

  const styleDefault = { ...style, };

  const onChangeWrapper = (e) => {
    const val = e.target.value;
    onChange(e, { id, value: val });
  };

  const onFocusWrapper = (e) => {
    if (typeof onFocus !== 'function') return;
    if (disabled) return;
    onFocus(e);
  };
  const onKeyDownWrapper = (e) => {
    if (typeof onKeyDown !== 'function') return;
    if (disabled) return;
    onKeyDown(e);
  };
  const onKeyUpWrapper = (e) => {
    if (typeof onKeyUp !== 'function') return;
    if (disabled) return;
    onKeyUp(e);
  };

  const onBlurWrapper = (e) => {
    if (typeof onBlur !== 'function') return;
    if (disabled) return;
    onBlur(e);
  };


  return (
    <textarea
      autoFocus={autoFocus}
      className={updatedClass}
      disabled={disabled}
      id={id}
      onBlur={onBlurWrapper}
      onChange={onChangeWrapper}
      onFocus={onFocusWrapper}
      onKeyDown={onKeyDownWrapper}
      onKeyUp={onKeyUpWrapper}
      placeholder={placeholder}
      style={styleDefault}
      value={value}
    ></textarea>
  );
}


Textarea.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  style: PropTypes.object,
  autoFocus: PropTypes.bool,
  fluid: PropTypes.bool,
  marginless: PropTypes.bool,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  size: types.size,
  disabled: PropTypes.bool,
  onKeyDown: PropTypes.func,
  onKeyUp: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
};

Textarea.defaultProps = {
  className: '',
  onChange: (x) => x,
  placeholder: '',
  style: {},
  fluid: true,
  size: 'size-xs',
};
