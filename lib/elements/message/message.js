import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { Block, Heading, Icon, Flexbox } from '../..';
import types from '../../types';


const ChatMessage = ({
  message,
  isYou,
  userName,
  time,
  status,
  id,
  style,
  className,
}) => {
  const styleDefault = { ...style };

  const defaultClasses = [
    'default-message',
    isYou && 'chat-message-your',
    !isYou && 'chat-message-another',
  ];

  const updatedClass = clsx(defaultClasses, className);

  return (
    <Block className={updatedClass}
      style={styleDefault}>
      <Heading as="h4"
        className="messege-heading-name">
        {userName}
      </Heading>

      <p>{message}</p>

      <div>
        <p className="messege-time">{time}</p>

        <Flexbox center>
          <Icon
            className={status ? 'blue' : 'gray'}
            icon={'check'}
          />
        </Flexbox>
      </div>

      <div id={id}></div>
    </Block>
  );
};


function Message({
  children,
  isInfo,
  isError,
  isSuccess,
  isWarning,
  iconName,
  className,
  style,
  size,
  fluid,
  isShadow,
}) {
  const defaultClasses = [
    'default-message',
    iconName !== '' && 'default-message-wrapper-flex',
    isInfo && 'info-message' && 'blue',
    isError && 'error-message',
    isSuccess && 'success-message',
    isWarning && 'warning-message',
    fluid && 'message-fluid',
    isShadow && 'message-shadow',
  ];

  const updatedClasses = clsx(defaultClasses, className);

  const defaultStyles = { ...style };

  return (
    <>
      <Block className={updatedClasses}
        style={defaultStyles}>
        {iconName && (
          <div className="icon-message-wrapper">
            <Icon icon={iconName}
              size={size} />
          </div>
        )}

        <div>{children}</div>
      </Block>
    </>
  );
}

export { Message, ChatMessage };

ChatMessage.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  isYou: PropTypes.bool,
  userName: PropTypes.string,
  time: PropTypes.string,
  status: PropTypes.bool,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

ChatMessage.defaultProps = {
  style: {},
  className: '',
  isYou: false,
  userName: '',
  time: '',
  status: false,
};

Message.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  isInfo: PropTypes.bool,
  isError: PropTypes.bool,
  isSuccess: PropTypes.bool,
  isWarning: PropTypes.bool,
  fluid: PropTypes.bool,
  isShadow: PropTypes.bool,
  iconName: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
  size: types.size,
};

ChatMessage.defaultProps = {
  isInfo: false,
  isError: false,
  isSuccess: false,
  isWarning: false,
  fluid: false,
  isShadow: false,
  className: '',
  style: {},
  size: 'size-l',
};
