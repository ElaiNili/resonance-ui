import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';
import types from '../../types';

function Switch({
  id,
  style,
  className,
  disabled,
  checked,
  onChange,
  label,
  position,
  size,
  fluid,
}) {

  const onChangeWrapper = (e) => {
    if (!disabled) {
      onChange(e, { id, checked, disabled, label, position, size, style });
    }
  };

  const switchClasses = clsx([
    'switch-wrap',
    disabled && 'switch-wrap-disabled',
    fluid && 'switch-wrap-fluid',
    position && `switch-${position}`,
    size && `${size}`,
  ]);

  const updatedClassName = clsx(switchClasses, className);

  return (
    <div
      className={updatedClassName}
      key={id}
      onClick={onChangeWrapper}
      style={style}
    >
      {label && <label>{label}</label>}

      <input
        checked={checked}
        className={'switch-input'}
        readOnly
        type='checkbox'
      />

      <div className='switch'>
        <div className='switch-pointer'></div>
      </div>
    </div>
  );
}

Switch.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  disabled: PropTypes.bool,
  checked: PropTypes.bool,
  onChange: PropTypes.func,
  label: PropTypes.string,
  position: PropTypes.oneOf(['left', 'top', 'right', 'bottom']),
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  size: types.size,
  fluid: PropTypes.bool,
};

Switch.defaultProps = {
  className: '',
  style: {},
  checked: false,
  onChange: (x) => x,
  label: null,
  id: '',
  size: 'size-xs',
};

export default Switch;
