export default {
  error: {
    offline: 'offline',
    fiasco: 'fiasco',
    'limit of attempts reached': 'limitOfAttemptsReached',
    'request timeout': 'requestTimeout',
    'login required': 'loginRequired',
    'payment required': 'paymentRequired',
    'duplicate email': 'duplicateEmail',
    'duplicate nickname': 'duplicateNickname',
    'blacklisted email': 'blacklistedEmail',
    'invalid email': 'invalidEmail',
    'invalid code': 'invalidCode',
    'invalid login or password': 'invalidLoginOrPassword',
    'invalid promocode': 'invalidPromocode',
    'invalid password': 'invalidPassword',
    'email not found': 'emailNotFound',
    'request limit reached': 'requestLimitReached',
    'select pay system': 'selectPaySystem',
    pay: 'pay',
  },
  success: {
    online: 'online',
    promoSuccess: 'promoSuccess',
  },
  labels: {
    // error
    offline: 'OFFLINE: Сеть пропала!',
    timeout: 'Превышено время ожидания запроса. Попробуйте еще раз.',
    limitOfAttemptsReached: 'Достигнут лимит попыток',
    fiasco: 'Это фиаско',
    requestTimeout: 'Превышено время ожидания запроса. Попробуйте еще раз.',
    loginRequired: 'Необходима авторизация',
    paymentRequired: 'Необходима оплата',
    duplicateEmail: 'Почта уже существует',
    duplicateNickname: 'Имя уже существует',
    blacklistedEmail: 'Мы не работает с временными почтовыми ящиками',
    invalidEmail: 'Некорректный почтовый адрес',
    invalidCode: 'Неверный код',
    invalidLoginOrPassword: 'Неверный логин или пароль',
    invalidPromocode: 'Не валидный промокод',
    invalidPassword: 'Не валидный пароль',
    emailNotFound: 'Почта не найдена',
    requestLimitReached: 'Достигнут лимит запросов',
    selectPaySystem: 'Выберете платёжную систему',
    pay: 'Ожидаем оплату',
    // success
    online: 'Ты снова в онлайн!',
    promoSuccess: 'Скидка применена',
    // exchanges
    binance: 'Binance',
    bitfinex: 'Bitfinex',
    bitstamp: 'Bitstamp',
    bittrex: 'Bittrex',
    coinbase: 'Coinbase Pro',
    coinbasepro: 'Coinbase Pro',
    hitbtc: 'HitBTC',
    huobi: 'Huobi Pro',
    huobipro: 'Huobi Pro',
    kraken: 'Kraken',
    okex: 'OKX',
    poloniex: 'Poloniex',
    ftx: 'FTX',
    bithumb: 'Bithumb',

  },
  modal: {
    fiasco: {
      message: 'Фиаско',
      buttons: [
        { id: 'submit', content: 'Написать в поддержку', filling: true, href: 'https://t.me/rinvestfund' },
      ],
    },
    stat: {
      message: 'Таймфреймы\n **М30, H4**\n доступны в платной версии\n Market Stat и Market Delta',
      buttons: [
        { id: 'free', content: 'Продолжить бесплатно', outlined: true, href: '' },
        { id: 'submit', content: 'Перейти к тарифам', filling: true, href: '/account/#prices' },
      ],
    },
    delta: {
      message: 'Оценивай динамику объемов на рынке\n **в платной версии Market Delta**',
      buttons: [
        { id: 'free', content: 'Продолжить бесплатно', outlined: true, href: '' },
        { id: 'submit', content: 'Перейти к тарифам', filling: true, href: '/account/#prices' },
      ],
    },
    marketDelta: {
      message: 'Оценивай динамику объемов на рынке\n **в платной версии Market Delta**',
      buttons: [
        { id: 'submit', content: 'Перейти к тарифам', filling: true, href: '/account/#prices' },
      ],
    },
    marketDeltaLogin: {
      message: 'Необходимо войти',
      buttons: [
        { id: 'free', content: 'На главную', outlined: true, href: '/' },
        { id: 'submit', content: 'Войти', filling: true, href: '/auth/#auth?login' },
      ],
    },
    screener: {
      message: '**Таймфреймы, Детализация, Сетки**\nдоступны в платной версии Screener',
      buttons: [
        { id: 'free', content: 'Продолжить бесплатно', outlined: true, href: '' },
        { id: 'submit', content: 'Перейти к тарифам', filling: true, href: '/account/#prices' },
      ],
    },
    login: {
      message: 'Необходимо войти',
      buttons: [
        { id: 'free', content: 'Отмена', outlined: true, },
        { id: 'submit', content: 'Войти', filling: true, href: '/auth/#auth?login' },
      ],
    },
  },

};
