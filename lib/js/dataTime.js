import {
  timeFormat, timeSecond, timeMinute, timeHour, timeDay, timeMonth, timeYear,
  timeWeek
} from 'd3';

import { timeFormatDefaultLocale } from 'd3-time-format';


function formatDataTime(data, lang = 'en') {

  if (lang === 'ru') {
    timeFormatDefaultLocale({
      'dateTime': '%A, %e %B %Y г. %X',
      'date': '%d.%m.%Y',
      'time': '%H:%M:%S',
      'periods': ['AM', 'PM'],
      'days': ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
      'shortDays': ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
      'months': ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'],
      'shortMonths': ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
    });
  } else {

    timeFormatDefaultLocale({
      'dateTime': '%x, %X',
      'date': '%d.%m.%Y',
      'time': '%H:%M:%S',
      'periods': ['AM', 'PM'],
      'days': ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
      'shortDays': ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
      'months': ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
      'shortMonths': ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    });
  }
  // const _isRussianTime = () => {
  //     const dataStart = `${data}`;
  //     const month = dataStart.slice(4, 7);
  //     return /[а-я]/i.test(month);
  // };

  const formatMillisecond = timeFormat('.%L'),
    formatSecond = timeFormat(':%S'),
    formatMinute = timeFormat('%H:%M'),
    formatHour = timeFormat('%H:%M'),
    formatDay = timeFormat('%a %d'),
    formatWeek = timeFormat(lang === 'ru' ? '%d %b %Y' : '%b %d %Y'),
    formatMonth = timeFormat('%b'),
    formatYear = timeFormat('%b %Y');


  function multiFormat(date) {
    return (timeSecond(date) < date ? formatMillisecond
      : timeMinute(date) < date ? formatSecond
        : timeHour(date) < date ? formatMinute
          : timeDay(date) < date ? formatHour
            : timeMonth(date) < date
              ? (timeWeek(date) < date ? formatDay : formatWeek)
              : timeYear(date) < date ? formatYear
                : formatMonth)(date);
  }

  return multiFormat(data);
}


export default formatDataTime;
