

const asyncMap = (items, fn, done) => {
  const len = items.length;
  if (!len) {
    done(null, []);
    return;
  }
  let errored = false;
  let count = 0;
  const result = new Array(len);

  const next = (index, err, value) => {
    if (errored) return;
    if (err) {
      errored = true;
      done(err);
      return;
    }
    result[index] = value;
    count++;
    if (count === len) done(null, result);
  };

  for (let i = 0; i < len; i++) {
    fn(items[i], next.bind(null, i), i);
  }
};

export default asyncMap;

// ------------ example --------

/*

const dataSet = new Array(10).fill(1);
const bl = (cur, cb) => {
    let smth = cur + 1;
    cb(null, smth);
}
const doneFn = (err, res) => {
    if (err) new Error(err);
    console.error('done', res)
}
asyncMap(dataSet, bl, doneFn);

*/
