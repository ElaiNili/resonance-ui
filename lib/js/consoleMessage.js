

function consoleMessage() {
  console.info('\n\n\n%c Resonance Holding Inc ', 'background: #c4c4c4; color: #017DFB; font-size: 30px');
  console.info('%c Нашли ошибку? Пишите в телеграм-чат ', 'background: #c4c4c4; color: #017DFB; font-size: 20px');
  console.info('%c https://t.me/rinvestfund', 'font-size: 20px');
  console.info('%c Спасибо! \n\n\n', 'background: #c4c4c4; color: #017DFB; font-size: 20px');
  return null;
}

export default consoleMessage;
