import text from './text.js';
import numbers from './numbers.js';
import debounce from './debounce.js';
import throttle from './throttle.js';
import asyncMap from './asyncMap.js';
import formatDataTime from './dataTime.js';
import * as isURLValid from './url.js';
import consoleMessage from './consoleMessage.js';
import screenSize from './screenSize.js';
import colorGenerator from './colorGenerator.js';
import time from './time.js';

export default {
  ...text, ...numbers, debounce,
  throttle, asyncMap,
  formatDataTime,
  ...isURLValid,
  consoleMessage,
  screenSize,
  colorGenerator,
  ...time,
};
