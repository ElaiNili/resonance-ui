function screenSize() {

  const width = Math.min(window.screen.width,
                         window.innerWidth,
                         document.documentElement.clientWidth);
  const height = Math.min(window.screen.height,
                          window.innerHeight,
                          document.documentElement.clientHeight);

  return { width, height };
}

export default screenSize;
