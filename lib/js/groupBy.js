const groupedBy = (array = [], groupParam = '') => {

  // используется структура Map - ключ-значение, которое в конце преобразуется в массив
  const groupedData = [
    ...array.reduce(
      (map, item) => {
        // для каждого элемента item в массиве:
        // берем значение поле, которое будем суммировать
        const { } = item;
        // берем значение ключа (поля по которому группируем)
        const key = item[groupParam];
        // находим существующий элемент в мапе
        const prev = map.get(key);

        if (prev) {
          // let askVol = prev.askVolume > askVolume ? prev.askVolume : askVolume;
        } else {
          map.set(key, Object.assign({}, item)); // если не нашли, то добавляем элемент в мап
        }

        return map;
      },
      new Map() // в начале пустой Map
    ).values() // возвращает итератор объектов, поэтому вначале поставили деструктуризацию - чтобы превратить все в красивый массив
  ];
  return groupedData;
};

export default groupedBy;
