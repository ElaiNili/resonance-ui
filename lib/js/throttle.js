// function throttle(fn, timeout) {
//   return function(args) {
//     console.log(this);
//     const previousCall = this.lastCall;
//     this.lastCall = Date.now();
//     if (previousCall === undefined // function is being called for the first time
//             || (this.lastCall - previousCall) > timeout) { // throttle time has elapsed
//       fn(args);
//     }
//   };
// }

// function throttle(func, ms) {

//   let isThrottled = false,
//     savedArgs,
//     savedThis;

//   function wrapper() {

//     if (isThrottled) { // (2)
//       savedArgs = arguments;
//       savedThis = this;
//       return;
//     }

//     func.apply(this, arguments); // (1)

//     isThrottled = true;

//     setTimeout(() => {
//       isThrottled = false; // (3)
//       if (savedArgs) {
//         wrapper.apply(savedThis, savedArgs);
//         savedArgs = savedThis = null;
//       }
//     }, ms);
//   }

//   return wrapper;
// }

function throttle(func, timeFrame) {
  let lastTime = 0;
  return function (...arg) {
    const now = Date.now();
    console.log(now, lastTime, timeFrame);
    if (now - lastTime >= timeFrame) {
      func(...arg);
      lastTime = now;
    }
  };
}

export default throttle;
