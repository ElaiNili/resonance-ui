import moment from 'moment';

const toLocalTime = (time, format) => {
  const f = format ?? 'YYYY-MM-DDTHH:mm:ss.SSSZ';
  return moment(moment.utc(time), f).local().format('YYYY-MM-DD HH:mm:ss');
};


export default { toLocalTime };
