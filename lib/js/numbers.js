import { format } from 'd3';
import { formatLocale } from 'd3-format';

Number.prototype.noExponents = function() {
  const data = String(this).split(/[eE]/);
  if (data.length === 1) return data[0];

  let z = '';
  const sign = this < 0 ? '-' : '';
  const str = data[0].replace('.', '');
  let mag = Number(data[1]) + 1;

  if (mag < 0) {
    z = sign + '0.';
    while (mag++) z += '0';
    return z + str.replace(/^\\-/, '');
  }
  mag -= str.length;
  while (mag--) z += '0';
  return str + z;
};


const trimZero = format('~');

const numberFormat = formatLocale({
  decimal: '.',
  thousands: ' ',
  grouping: [3],
  percent: '%',
});


function formatVolume(num = 0, isPercent = false,) {
  const value = isNaN(num) ? console.error('Value is not number') : Number(num);

  const formatsNumber = numberFormat.format(',~r');
  const formatsPercent = numberFormat.format(',.2%');
  return isPercent ? formatsPercent((value / 100)) : formatsNumber(value);
}


function formatVolumeData(rawNumber) {

  const num = Math.abs(rawNumber);

  const digitsAfterDot = (num) => format(`.${num}f`);

  let digits = 0;
  if (num < 0.1) digits = 5;
  else if (num < 1) digits = 4;
  else if (num < 10) digits = 3;
  else if (num < 100) digits = 2;
  else if (num < 1000) digits = 0;
  else digits = 0;

  const noZeros = Number(trimZero(num));
  const result = Number(digitsAfterDot(digits)(noZeros));
  return rawNumber > 0 ? result : result * -1;
}


function formatBigVolumeWrapper(number = 0, lastOneNum, lang) {

  const prefixArrEng = {
    'M': ' mln',
    'T': ' trln',
    'G': ' bln',
    'k': ' k',
  };

  const prefixArrRu = {
    'M': ' млн',
    'T': ' трлн',
    'G': ' млрд ',
    'k': ' тыс',
  };

  const newPrefix = lang === 'ru' ? prefixArrRu : prefixArrEng;
  let numbers;
  for (const key in newPrefix) {
    if (lastOneNum === key) {
      numbers = number.replace(lastOneNum, newPrefix[key]);
    }
  }
  return numbers;
}


function formatBigVolume(num = 0, isPercent = false, lang = 'ru') {

  const value = isNaN(num) ? console.error('Value is not number') : Number(num);

  const arrUniquePrefix = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'm'];

  const formatsNumber = numberFormat.format(',.3s');
  const formatsPercent = numberFormat.format(',.2%');

  const numbers = isPercent
    ? formatsPercent((value / 100))
    : formatsNumber(value);
  const lastOneNum = numbers.toString().split('').pop();

  if (isPercent) return numbers;

  for (let i = 0; i < arrUniquePrefix.length; i++) {
    if (arrUniquePrefix[i] === lastOneNum) {
      return formatVolume(value);
    }
  }
  return formatBigVolumeWrapper(numbers, lastOneNum, lang);
}


export default { formatVolume, trimZero, formatVolumeData, formatBigVolume };
