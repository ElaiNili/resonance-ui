import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import types from '../../types/index.js';

export default function Statistics({
  className,
  style,
  value,
  label,
  size,
  labelAlign,
  textAlign,
  center,
}) {
  const defaultClasses = [
    'statistics',
    size && `${size}`,
    textAlign !== '' && `text-align-${textAlign}`,
    center &&  'statistics-center statistics-label-center',
  ];
  const updatedClass = clsx(defaultClasses, className);

  const labelDefaultClasses = [
    labelAlign !== '' && `text-align-${labelAlign}`,
  ];



  return (
    <div className={updatedClass}
      style={style}>
      <div>{value}</div>
      <label className={labelDefaultClasses}>{label}</label>
    </div>
  );
}



Statistics.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  labelAlign: PropTypes.string,
  textAlign: PropTypes.string,
  size: types.size,
  center: PropTypes.bool
};

Statistics.defaultProps = {
  className: '',
  style: {},
  value: '',
  label: '',
  labelAlign: '',
  textAlign: '',
};
