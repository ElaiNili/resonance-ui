import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';



function Card({
  children,
  className,
  style,
  width,
  notBorder,
  id,
}) {

  const defaultClasses = [
    'card',
    notBorder && ' ',
  ];

  const updatedClasses = clsx(defaultClasses, className);

  const cardWidthStyle = {
    width,
  };

  const defaultStyles = { ...style, ...cardWidthStyle };


  return (
    <div
      className={updatedClasses}
      id={id}
      style={defaultStyles}
    >
      {children}
    </div>
  );
}

function CardContent({
  children,
  className,
  style,
  center,
  notImage,
}) {

  const defaultClasses = [
    'card-content',
    center && '',
    notImage && '',
  ];

  const updatedClasses = clsx(defaultClasses, className);

  return (
    <div
      className={updatedClasses}
      style={style}>
      {children}
    </div>
  );
}

export { Card, CardContent };

Card.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  width: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  notBorder: PropTypes.bool,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

Card.defaultProps = {
  children: null,
  className: '',
  style: {},
  width: '200',
  notBorder: false,
  id: '',
};

CardContent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  center: PropTypes.bool,
  notImage: PropTypes.bool,
};

CardContent.defaultProps = {
  children: null,
  className: '',
  style: {},
  center: false,
  notImage: false,
};
