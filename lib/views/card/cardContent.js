import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import { Heading } from '../../../lib';


function CardHeader({ children, className, style, as }) {

  const defaultClasses = [
    'card-header',
  ];

  const updatedClasses = clsx(defaultClasses, className);

  const defaultStyles = { ...style };
  return (
    <>
      <Heading as={as}
        className={updatedClasses}
        style={defaultStyles}>{children}</Heading>
    </>
  );
}

function CardMeta({ children, className, style }) {

  const defaultClasses = [
    'card-meta-text-color',
  ];

  const updatedClasses = clsx(defaultClasses, className);

  const defaultStyles = { ...style };
  return (
    <p className={updatedClasses}
      style={defaultStyles}>{children}</p>
  );
}

function CardDescription({ children, className, style }) {

  const defaultClasses = [
    'card-description-text-color',
  ];

  const updatedClasses = clsx(defaultClasses, className);

  const defaultStyles = { ...style };

  return (
    <p className={updatedClasses}
      style={defaultStyles}>{children}</p>
  );
}

export { CardHeader, CardMeta, CardDescription };

CardHeader.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  as: PropTypes.string,
};

CardHeader.defaultProps = {
  children: null,
  className: '',
  style: {},
  as: 'h4',
};

CardMeta.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
};

CardMeta.defaultProps = {
  children: null,
  className: '',
  style: {},
};

CardDescription.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
};

CardDescription.defaultProps = {
  children: null,
  className: '',
  style: {},
};
