import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';


function CardGroup({ children, className, style, horizontal, vertical }) {

  const defaultClasses = [
    'default-card-group',
    horizontal && 'card-group-horizontal',
    vertical && 'card-group-vertical',
  ];

  const updatedClasses = clsx(defaultClasses, className);

  const styleDefault = { ...style };

  return (
    <div className={updatedClasses}
      style={styleDefault}>{children}</div>
  );
}

export default CardGroup;

CardGroup.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  horizontal: PropTypes.bool,
  vertical: PropTypes.bool,

};

CardGroup.defaultProps = {
  children: null,
  className: '',
  style: {},
  horizontal: false,
  vertical: false,
};
