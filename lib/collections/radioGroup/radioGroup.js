import React from 'react';
import PropTypes from 'prop-types';

import Flexbox from '../../elements/flexbox/flexbox';
import RadioButton from '../../elements/radio';

export default function RadioGroup({
  items,
  selected,
  onClick,
  column,
  start,
  end,
  center,
  spaceBetween,
  spaceAround,
  top,
  bottom,
  middle,
  stretch,
  baseline,
  style,
  disabled,
  nowrap,
}) {

  const onRadioClick = (e, p) => {
    if (disabled) return;
    if (onClick !== undefined) onClick(e, p);
  };

  return (
    <div style={style}
      test_id='RadioGroup'>
      {items && items.length > 0 && (
        <Flexbox
          baseline={baseline}
          bottom={bottom}
          center={center}
          column={column}
          end={end}
          middle={middle}
          nowrap={nowrap}
          spaceAround={spaceAround}
          spaceBetween={spaceBetween}
          start={start}
          stretch={stretch}
          top={top}
        >
          {items.map((el) => (
            <RadioButton
              active={selected === el.id}
              {...el}
              key={el.id}
              onClick={onRadioClick}
            />
          ))}
        </Flexbox>
      )}
    </div>
  );
}

RadioGroup.propTypes = {
  items: PropTypes.array,
  selected: PropTypes.string,
  onClick: PropTypes.func,
  column: PropTypes.bool,
  start: PropTypes.bool,
  end: PropTypes.bool,
  center: PropTypes.bool,
  spaceBetween: PropTypes.bool,
  spaceAround: PropTypes.bool,
  top: PropTypes.bool,
  bottom: PropTypes.bool,
  middle: PropTypes.bool,
  stretch: PropTypes.bool,
  baseline: PropTypes.bool,
  style: PropTypes.object,
  disabled: PropTypes.bool,
  nowrap: PropTypes.bool,
};

RadioGroup.defaultProps = {
  column: false,
  nowrap: false,
};
