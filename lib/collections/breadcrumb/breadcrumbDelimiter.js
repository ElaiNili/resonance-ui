import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import { Icon } from '../../index';


export default function BreadcrumbDelimiter({
  icon,
  size,
  children,
  content,
  className,
  style,
}) {

  // ================================================================
  const defaultClasses = [
    'default-breadcrumb-delimiter-wrapper',
    'center-icon-marker',

  ];

  const updatedClasses = clsx(defaultClasses, className);

  // ================================================================

  const defaultIconClasses = [
    'center-icon-marker',
    'default-cursor',
  ];

  const updatedIconClasses = clsx(defaultIconClasses);

  // ================================================================

  return (
    <div className={updatedClasses}
      style={style}>
      {icon !== '' ? (
        <Icon className={updatedIconClasses}
          icon={icon}
          size={size} />
      ) : (
        <div>{children || content}</div>
      )}
    </div>
  );
}

BreadcrumbDelimiter.propTypes = {
  icon: PropTypes.string,
  size: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string,
  style: PropTypes.object,
};

BreadcrumbDelimiter.defaultProps = {
  icon: '',
  size: 'size-l',
  isLast: false,
  children: null,
  content: null,
  className: '',
  style: {},
};
