import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

export default function Breadcrumb({ children, style, className }) {
  const defaultUlClasses = [
    'default-breadcrumb-ul',
  ];


  const updatedUlClasses = clsx(defaultUlClasses, className);

  return (
    <div className={updatedUlClasses}
      style={style}>{children}</div>
  );
}

Breadcrumb.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
};

Breadcrumb.defaultProps = {
  children: null,
  className: '',
  style: {},

};


