import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import { Icon } from '../../index';

export default function BreadcrumbItem({
  icon,
  size,
  children,
  content,
  path,
  isLast,
  className,
  style,
}) {
  const defaultClasses = [
    'default-breadcrumb-item',
    isLast && 'breadcrumb-last-item',
  ];

  const updatedClasses = clsx(defaultClasses, className);

  return (
    <div className={updatedClasses}
      style={style}>
      {icon === '' ? (
        <a href={path}>{children || content}</a>
      ) : (
        <Icon href={path}
          icon={icon}
          size={size} />
      )}
    </div>
  );
}

BreadcrumbItem.propTypes = {
  icon: PropTypes.string,
  size: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  path: PropTypes.string,
  isLast: PropTypes.bool,
  className: PropTypes.string,
  style: PropTypes.object,
};

BreadcrumbItem.defaultProps = {
  icon: '',
  size: 'size-l',
  children: null,
  content: null,
  path: '#',
  isLast: false,
  className: '',
  style: {},
};
