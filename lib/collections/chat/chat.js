import React, { useState } from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';

import { ChatMessage, Textarea, Form, Button } from '../../index';


// how to scroll chat to bottom
// https://rawgit.com/WICG/ResizeObserver/master/examples/chat.html

export default function Chat({
  messArr,
  bgColor,
  style,
  className,
  onSubmit,
  telegram,
  isRoundCourse,
  placeholder,
  autoFocus,
  icon,
  text,
  id,

}) {
  const [mess, setMess] = useState('');

  const bgColorObj = {
    backgroundColor: bgColor,
  };

  const styleDefault = { ...style, ...bgColorObj };

  const defaultClasses = [
    'default-chat',
    telegram && 'telegram-form-chat',
    isRoundCourse && 'chat-round-course'
  ];

  const updatedClass = clsx(defaultClasses, className);


  const onSubmitWrapper = (e) => {
    e.preventDefault();
    onSubmit(e, { id, mess });
  };

  const onChangeWrapper = (e, { value }) => {
    setMess(value);
  };

  return <p>coming soon</p>;

  // return (
  //   <>
  //     <div className={updatedClass}
  //       style={styleDefault}>
  //       {messArr.map((item, i) => {
  //         const { message, isYou, userName, time, status, id } = item;
  //         return (
  //           <ChatMessage
  //             id={id}
  //             isYou={isYou}
  //             key={i}
  //             message={message}
  //             status={status}
  //             time={time}
  //             userName={userName}
  //           />
  //         );
  //       })}
  //     </div>
  //     <Form onSubmit={onSubmitWrapper} >
  //       <Textarea
  //         autoFocus={autoFocus}
  //         isRoundCourse={true}
  //         onChange={onChangeWrapper}
  //         placeholder={placeholder}
  //         value={mess} />
  //       <Button icon={icon}
  //         type="submit">{text}</Button>
  //     </Form>

  //   </>
  // );
}


Chat.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  messArr: PropTypes.array,
  bgColor: PropTypes.object,
  style: PropTypes.object,
  className: PropTypes.string,
  onSubmit: PropTypes.func,
  telegram: PropTypes.bool,
  isRoundCourse: PropTypes.bool,
  placeholder: PropTypes.string,
  autoFocus: PropTypes.bool,
  icon: PropTypes.string,
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

Chat.defaultProps = {
  messArr: [],
  bgColor: null,
  style: {},
  className: '',
  onSubmit: (e) => e,
  telegram: false,
  isRoundCourse: false,
  placeholder: 'message...',
  autoFocus: false,
  icon: 'send',
  text: '',
};
