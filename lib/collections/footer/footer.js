import React, { useMemo } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';


import Button from '../../elements/button/button.js';
import Flexbox from '../../elements/flexbox/flexbox.js';

const buttons = [
  { href: '/tos/', content: 'Terms of Service' },
  { href: '/cookie/', content: 'Cookie' },
  { href: '/privacy/', content: 'Privacy Policy' },
];

const hrefs = [
  {
    href: 'https://www.linkedin.com/authwall?trk=ripf&trkInfo=AQGZvyIWNE0usgAAAXv0d1c4GOJ7fLg1xt9QsVPYbX9fyFPfp-lc7aRHv7JnznwQ3GsEDgrejul72Ae4dkIq4_mK3qWW76pprJeHi61DfL5XLK8NCGIchp96LvgNqf6PMnq2q-8=&originalReferer=&sessionRedirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2Fresonanceholding%2F',
    alt: 'linkedin', icon: 'linkedin'
  },
  { href: 'https://www.instagram.com/resonanceholding/', alt: 'instagram', icon: 'instagram' },
  { href: 'https://www.facebook.com/resonanceholding', alt: 'facebook', icon: 'facebook-f' },
  { href: 'https://twitter.com/resonancehldg', alt: 'twitter', icon: 'twitter' },
  { href: 'https://t.me/rinvestfund', alt: 'telegram', icon: 'telegram' },
];

const currentYear = new Date().getFullYear();

export default function Footer({ maxWidth, style, className, full }) {

  const onClick = (e, { href }) => window.open(href, '_blank');

  const links = useMemo(() => hrefs.map((href, i) => (
    <Button basic
      href={href.href}
      icon={href.icon}
      iconType='fa'
      key={i}
      onClick={onClick} />
  )), []);

  const items = useMemo(() => buttons.map((el, i) => (
    <Button basic
      content={el.content}
      href={el.href}
      key={i}
      onClick={onClick}
      slim />
  )), []);

  const defaultClasses = [
    'footer',
  ];

  const updatedClass = clsx(defaultClasses, className);

  const footerClasses = [
    'footer-data',
    full && 'footer-divider',
  ];

  const footerClass = clsx(footerClasses);

  return (
    <div className={updatedClass}
      style={style}>
      <Flexbox center>
        {items}
      </Flexbox>
      <Flexbox center>
        {links}
      </Flexbox>
      <div className={footerClass}>
        © 2017-{currentYear} Resonance Holding Inc. All rights reserved.
      </div>
    </div>
  );
}

Footer.propTypes = {
  maxWidth: PropTypes.string,
  full: PropTypes.bool,
};

Footer.defaultProps = {
  full: false,
};

