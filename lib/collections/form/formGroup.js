import React from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';

function FormGroup({
  horizontal,
  children,
  className,
  style,
}) {

  const classesDefault = [
    horizontal && 'form-group-horizontal'
  ];

  const updatedClasses = clsx(classesDefault, className);

  return (
    <div className={updatedClasses}
      style={style}>
      {children}
    </div>
  );
}

export default FormGroup;

FormGroup.propTypes = {
  horizontal: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,

};

FormGroup.defaultProps = {
  horizontal: false,
  children: null,
  className: '',
  style: {},
};
