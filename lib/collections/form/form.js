import React from 'react';

import PropTypes from 'prop-types';

export default function Form({
  children,
  className,
  style,
  onSubmit,
  id,
}) {

  const onSubmitWrapper = (e) => {
    e.preventDefault();
    onSubmit(e, { id: e.target.id });
  };

  return (
    <form
      className={className}
      id={id}
      onSubmit={onSubmitWrapper}
      style={style}
    >
      {children}
    </form>
  );
}

Form.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  onSubmit: PropTypes.func,
  id: PropTypes.string,
};

Form.defaultProps = {
  children: null,
  className: '',
  style: {},
  onSubmit: (e) => e,
  id: '',
};



