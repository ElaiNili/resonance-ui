import React, {  useState } from 'react';

import PropTypes from 'prop-types';

import { Button, Checkbox } from '../../index';
import types from '../../types';


export default function FormCheckbox({
  children,
  path,
  content,
  icon,
  size,
  className,
  style,
  // type,
}) {

  const [isChecked, setIsChecked] = useState(false);
  // const [buttonType, setButtonType] = useState(null);

  // useEffect(() => {
  // const btnType = new Map(type, true);

  // const btnType = { `${type}`: true };
  // setButtonType(btnType);
  // }, [type]);

  const onChangeCheckbox = () => {
    setIsChecked((s) => !s);
  };

  return (
    <div className={className}
      style={style}>
      {children}

      <label className="checkbox-form-label">
        <Checkbox checked={isChecked}
          className="checkbox-form-checkbox"
          onChange={onChangeCheckbox} />

        <p>I agree to the <a href={path}> Terms and Conditions</a></p>
      </label>

      {icon === '' ? (
        <Button disabled={!isChecked}
          type="submit">
          {content}
        </Button>
      ) : (
        <Button buttonSize={size}
          disabled={isChecked}
          // {...buttonType}
          icon={icon}
          style={{ marginLeft: '0' }}
          type="submit"
        >
          {content}
        </Button>
      )}
    </div>
  );
}

FormCheckbox.propTypes = {
  children: types.children,
  path: PropTypes.string,
  content: PropTypes.string,
  icon: PropTypes.string,
  size: types.size,
  className: PropTypes.string,
  style: PropTypes.object,
  type: PropTypes.oneOf([
    undefined,
    'basic',
    'filling',
    'outlined',
  ])
};

FormCheckbox.defaultProps = {
  children: null,
  path: '',
  content: '',
  icon: '',
  size: 'size-xs',
  className: '',
  style: {},
  type: 'filling',
};
