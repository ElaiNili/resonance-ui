import React from 'react';
import { Block, Flexbox, Input } from '../../index';
import PropTypes from 'prop-types';


export default function FormInputs({ inputArr,  }) {

  if (!inputArr) return null;

  return (
    <Block>
      {/* <Flexbox> */}
      {inputArr.map(((d, i) =>
        // console.log(d);
        (
          <Input
            {...d}
            key={d.id || i}
          />
        )
      ))}
      {/* </Flexbox> */}
    </Block>
  );

}

FormInputs.propTypes = {
  inputArr: PropTypes.array,
};

FormInputs.defaultProps = {
  inputArr: null,
};
