import React, { useState, useMemo } from 'react';

// import clsx from 'clsx';
import PropTypes from 'prop-types';

import types from '../../types';

import { List, ListItem } from '../../index.js';
import Icon from '../../elements/icon/icon';
import Flexbox from '../../elements/flexbox/flexbox';


function ListNode({ data, listItem, marker }) {

  // const [isOpen, setIsOpen] = useState(false);

  const isMarker = Array.isArray(data[0][1]) ? marker : 'none';

  const listItems = data.map((item, i) => {
    const [name, d] = Array.isArray(item) ? item : [item];

    const isArr = Array.isArray(d) ? (
      <>
        {name}

        <ListNode data={d}
          listItem={listItem}
          marker={marker} />
      </>
    ) : d === undefined ? name[listItem] : name;


    return (
      <ListItem key={i}>
        {isArr}
      </ListItem>
    );
  });

  return (
    <List className='tree'
      marker={isMarker}>
      {listItems}
    </List>

  );
}

ListNode.propTypes = {
  listItem: PropTypes.object,
  marker: PropTypes.string,
  data: PropTypes.array,
};

function Tree({ data }) {

  const [openedItems, setOpenedItems] = useState([]);

  const isOpened = (id, arr) => arr.some((d) => d === id);

  const marker = openedItems ? 'open' : 'closed';

  const expandCollapseList = (e, { id }) => {
    setOpenedItems((s) => (isOpened(id, s)
      ? s.filter((d) => d !== id)
      : [...s, id]));
  };

  const listItems = useMemo(() => data.map((item, i) => {
    const [name, d] = Array.isArray(item) ? item : [item];

    const isArr =  Array.isArray(d);

    const node = isArr ? (
      <>
        {name}

        {isOpened(name, openedItems) && (
          <ListNode data={d}
            listItem={'tickerUnified'}
            marker={marker} />
        )}
      </>
    ) : name;

    const icon = isArr ? 'chevron_right' : null;

    return (
      <Flexbox key={i}>
        {icon && (
          <Icon icon={icon}
            size='size-m' />
        )}

        <ListItem icon={icon}
          id={name}
          onClick={expandCollapseList}>
          {node}
        </ListItem>
      </Flexbox>
    );
  }), [data, openedItems]);


  return (
    <List className='tree'
      marker={'none'}>
      {listItems}
    </List>
  );
}

Tree.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  children: types.children,
  className: PropTypes.string,
  style: PropTypes.object,

  data: PropTypes.array,
};

Tree.defaultProps = {
  id: '',
  className: '',
  style: {},

  data: [],
};

export default Tree;
