import React from 'react';

import PropTypes from 'prop-types';

import DesktopHeader from './desktopHeader';
import MobileHeader from './mobileHeader';

function AppHeader({ menuBlocks, additionalMenu, secondLogo, className, style, id, }) {
  return (
    <>
      <DesktopHeader additionalMenu={additionalMenu}
        className={className}
        id={id}
        menuBlocks={menuBlocks}
        secondLogo={secondLogo}
        style={style} />

      <MobileHeader additionalMenu={additionalMenu}
        className={className}
        id={id}
        menuBlocks={menuBlocks}
        style={style} />
    </>
  );
}

AppHeader.propTypes = {
  menuBlocks: PropTypes.array,
  additionalMenu: PropTypes.array,
  secondLogo: PropTypes.bool
};

AppHeader.defaultProps = {
  menuBlocks: [],
  additionalMenu: [],
};

export default AppHeader;
