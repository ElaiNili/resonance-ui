import React, { useMemo } from 'react';

import PropTypes from 'prop-types';

import {
  Menu,
  MenuLogo,
  LeftMenuItems,
  CenterMenuItems,
  RightMenuItems,
} from '../..';

import siteLogo from '../../img/siteLogo.svg';
import libLogo from '../../img/libLogo.svg';
import { MenuItemBuilder } from './builders';
import clsx from 'clsx';

function DesktopHeader({ menuBlocks, additionalMenu, secondLogo, className, style, id, }) {

  const rows = useMemo(() => (() => {
    let items = 1;
    if (menuBlocks.length > 0) {
      items++;
    }
    if (additionalMenu.length > 0) {
      items++;
    }
    return items;
  })(), [menuBlocks, additionalMenu]);

  // =======================================

  const defaultClasses = [
    'app-header-menu'
  ];

  const updatedClasses = clsx(defaultClasses, className);

  return (
    <div className={updatedClasses}
      id={id}
      style={style}>
      <Menu rows={rows}>
        <LeftMenuItems center
          key='logo'>
          <MenuLogo alt='Resonance'
            img={siteLogo} />
          {secondLogo && (
            <MenuLogo alt='libLogo'
              img={libLogo}
              style={{ marginLeft: '24px' }} />
          )}
        </LeftMenuItems>

        {menuBlocks.length > 0 && (
          <CenterMenuItems center
            key='menu'>
            <MenuItemBuilder data={menuBlocks} />
          </CenterMenuItems>
        )}

        {additionalMenu.length > 0 && (
          <RightMenuItems center
            inline
            key='additional'>
            <MenuItemBuilder data={additionalMenu} />
          </RightMenuItems>
        )}
      </Menu>
    </div>
  );
}

DesktopHeader.propTypes = {
  menuBlocks: PropTypes.array,
  additionalMenu: PropTypes.array,
  secondLogo: PropTypes.bool
};

DesktopHeader.defaultProps = {
  menuBlocks: [],
  additionalMenu: [],
};

export default DesktopHeader;
