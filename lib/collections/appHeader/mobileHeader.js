import React, { useEffect, useState } from 'react';

import PropTypes from 'prop-types';

import { Button, Menu, MenuLogo, RightMenuItems, Block } from '../..';

import siteLogo from '../../img/siteLogo.svg';

import { MobileItemBuilder, MobileItemBuilderSecond } from './builders';
import clsx from 'clsx';

function MobileHeader({ menuBlocks, additionalMenu, className, style, id }) {
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    const body = document.body;
    if (isOpen) body.classList.add('body-overflow-hidden');
    if (!isOpen) body.classList.remove('body-overflow-hidden');
  }, [isOpen]);

  const onHandelOpen = () => setIsOpen((s) => !s);

  const defaultClasses = [
    'mobile-header'
  ];

  const updatedClasses = clsx(defaultClasses, className);

  return (
    <Block basic
      className={updatedClasses}
      id={id}
      style={style}>
      <Menu rows={2}>
        <MenuLogo
          alt='Resonance'
          className='app-header-logo-mobile'
          img={siteLogo}
        />

        <RightMenuItems right>
          <Button
            basic
            className={'app-header-button-mob-menu-open'}
            icon={'menu'}
            onClick={onHandelOpen}
          />
        </RightMenuItems>
      </Menu>

      <Block
        basic
        className={`${
          isOpen ? '' : 'hidden-mobile-menu-overlay'
        } mobile-menu-overlay`}
        onClick={onHandelOpen}
      >
        <Block
          basic
          className={`${
            isOpen ? 'animation-mobile-menu-there' : 'hidden-mobile-menu'
          } mobile-menu-wrapper`}
          onClick={(e) => e.stopPropagation()}
        >
          <Button
            basic
            className={'mobile-menu-icon-close'}
            icon={'clear'}
            onClick={onHandelOpen}
          />

          <Block basic
            className={'mobile-menu-content-wrapper'}>
            <div className={'mobile-menu-main'}>
              {MobileItemBuilder(menuBlocks)}

              {MobileItemBuilderSecond(additionalMenu)}
            </div>
          </Block>
        </Block>
      </Block>
    </Block>
  );
}

MobileHeader.propTypes = {
  menuBlocks: PropTypes.array,
  additionalMenu: PropTypes.array,
};

MobileHeader.defaultProps = {
  menuBlocks: [],
  additionalMenu: [],
};

export default MobileHeader;
