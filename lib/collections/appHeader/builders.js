import React from 'react';
import {
  Dropdown,
  Button,
  MenuItem,
  ButtonGroup,
  List,
  ListItem,
  ListHeader,
} from '../..';

const MenuItemBuilder = ({ data }) => {

  const buttonsBuilder = (arr) => arr.items.map((btn, iter) => (
    <Button
      basic
      className='button-group-links'
      content={btn.label}
      fluid
      key={iter}
      left
      {...btn}
    />
  ));


  return data.map((item, i) => {
    // eslint-disable-next-line no-prototype-builtins
    const isDropdown = item.hasOwnProperty('items');
    return isDropdown ? (
      <MenuItem key={i}>
        <Dropdown
          autoClose
          basic
          content={item.content}
          fluid
          icon={item.icon}
          id={item.id}
          outSideClose
          // slimWidth
          width={170}
        >
          <ButtonGroup fluid
            vertical>
            {buttonsBuilder(item)}
          </ButtonGroup>
        </Dropdown>
      </MenuItem>
    ) : (
      <Button
        basic
        // fluid
        key={i}
        {...item}
      />
    );
  });
};

const MobileItemBuilder = (data = []) => {
  if (data.length === 0) return null;

  return data?.map((item, i) => {
    // eslint-disable-next-line no-prototype-builtins
    const isDropdown = item.hasOwnProperty('items');
    return isDropdown ? (
      <List key={i}
        marginless
        paddingless>
        <ListHeader as='h4'
          className={'mobile-menu-main-rtt-list-header'}>
          {item.content}
        </ListHeader>

        {item.items.map((btn, iter) => (
          <ListItem key={iter}
            secondary>
            <Button
              basic
              className='button-group-links'
              content={btn.label}
              fluid
              key={iter}
              {...btn}
            />
          </ListItem>
        ))}
      </List>
    ) : (
      <Button
        basic
        className={'mobile-menu-main-rtt-buttons'}
        fluid
        key={i}
        {...item}
      />
    );
  });
};

const MobileItemBuilderSecond = (data = []) => {
  if (data.length === 0) return null;

  return data?.map((item, i) => {
    // eslint-disable-next-line no-prototype-builtins
    const isDropdown = item.hasOwnProperty('items');
    return isDropdown ? (
      <ButtonGroup fluid
        key={item?.id}>
        {item.items.map((btn, iter) => (
          <Button basic
            content={btn.label}
            key={iter}
            {...btn} />
        ))}
      </ButtonGroup>
    ) : (
      <Button
        basic
        className={'mobile-menu-main-rtt-buttons'}
        fluid
        key={i}
        {...item}
      />
    );
  });
};

export { MenuItemBuilder, MobileItemBuilder, MobileItemBuilderSecond };
