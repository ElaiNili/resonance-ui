import React, { useState } from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

import { Table } from 'react-virtualized';
import Columns from './column';

import 'react-virtualized/styles.css';

export default function TableVirtualized({
  width,
  height,
  headerHeight,
  rowHeight,

  data,
  columnsData,

  id,

  className,
  headerClassName,
  rowClassName,
  gridClassName,

  style,
  headerStyle,
  gridStyle,
  rowStyle,

  textAlign,
  // ================
  disableHeader,
  noRowsRenderer,
  onColumnClick,
  onHeaderClick,
  onRowClick,
  onRowDoubleClick,
  onRowRightClick,

  overscanRowCount,
  onScroll,

  scrollToIndex,
  // scrollTop,
  defaultSortBy,
}) {


  const [direction, setDirection] = useState('DESC');
  const [by, setBy] = useState(defaultSortBy);

  // ==================================================
  const defaultClasses = [
    'table-virtualized-center',
    textAlign && `block-text-${textAlign}`,
    'table-virtualized-rowItem-without-margin',
  ];

  const updatedClasses = clsx(defaultClasses, className);
  // ==================================================

  const defaultHeaderClassName = [
    'header-column-no-margin',
  ];

  const updatedHeaderClassName = clsx(defaultHeaderClassName, headerClassName);

  // ==================================================

  const defaultGridClasses = [
    'table-grid-virtualized',
  ];

  const updatedGridClasses = clsx(defaultGridClasses, gridClassName);
  // ==================================================
  const defaultRowClasses = [
    '',
  ];

  const updatedRowClasses = clsx(defaultRowClasses, rowClassName);

  // ========================================

  const sortData = (arr, sortBy, sortDirection) => {
    if (arr.length === 0) return;
    if (sortBy === '') return;
    const key = sortBy;
    const isNan = isNaN(arr[0][key]);

    // ASC - number
    if (sortDirection === 'ASC') {
      arr.sort((a, b) => {
        if (Number(a[key]) > Number(b[key])) return 1;
        if (Number(a[key]) < Number(b[key])) return -1;
        return 0;
      });
    }
    // DESC - number
    if (sortDirection === 'DESC') {
      arr.sort((a, b) => {
        if (Number(a[key]) < Number(b[key])) return 1;
        if (Number(a[key]) > Number(b[key])) return -1;
        return 0;
      });
    }

    if (isNan) {
      // ASC - str
      if (sortDirection === 'ASC') {
        arr.sort((a, b) => {
          if (a[key] > b[key]) return 1;
          if (a[key] < b[key]) return -1;
          return 0;
        });
      }
      // DESC - str
      if (sortDirection === 'DESC') {
        arr.sort((a, b) => {
          if (a[key] < b[key]) return 1;
          if (a[key] > b[key]) return -1;
          return 0;
        });
      }
    }
  };

  const _sort = ({ sortBy, sortDirection }) => {
    setDirection(sortDirection);
    setBy(sortBy);
  };

  // useEffect(() => sortData(data, by, direction), [data, by, direction]);

  sortData(data, by, direction);
  // ========================================

  const columnsDataRender = Columns(columnsData);


  return (
    <Table
      className={updatedClasses}
      disableHeader={disableHeader}
      gridClassName={updatedGridClasses}
      gridStyle={gridStyle}
      headerClassName={updatedHeaderClassName}
      headerHeight={headerHeight}
      headerStyle={headerStyle}
      height={height}
      id={id}
      noRowsRenderer={noRowsRenderer}
      onColumnClick={onColumnClick}
      onHeaderClick={onHeaderClick}
      onRowClick={onRowClick}
      onRowDoubleClick={onRowDoubleClick}
      onRowRightClick={onRowRightClick}
      onScroll={onScroll}
      overscanRowCount={overscanRowCount}
      rowClassName={updatedRowClasses}
      rowCount={data.length}
      rowGetter={({ index }) => data[index]}
      rowHeight={rowHeight}
      rowStyle={rowStyle}
      scrollToIndex={scrollToIndex} // amount invisible div s
      sort={_sort}
      sortBy={by} //default scroll to 199
      // headerRowRenderer={} // обертка labels in header
      // onRowMouseOut={() => console.log('aga')} //?
      // onRowMouseOver={() => console.log('aga')} //?
      // scrollToAlignment={"start"}//?
      // scrollTop={scrollTop}
      // autoHeight={true} -
      // sort={(e) => { console.log(e) }}
      sortDirection={direction}
      style={style}
      width={width}
    >
      {columnsDataRender}
    </Table>
  );
}

TableVirtualized.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  width: PropTypes.number,
  height: PropTypes.number,
  headerHeight: PropTypes.number,
  rowHeight: PropTypes.number,
  columnsData: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      dataKey: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
      columnWidth: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
      cellRenderer: PropTypes.func,
      id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
      disableSort: PropTypes.bool,
    }),
  ),
  data: PropTypes.arrayOf(
    PropTypes.object,
  ),
  textAlign: PropTypes.oneOf([
    'left', 'center', 'right', undefined,
  ]),
  disableHeader: PropTypes.bool,
  noRowsRenderer: PropTypes.func,
  className: PropTypes.string,
  headerClassName: PropTypes.string,
  gridClassName: PropTypes.string,
  rowClassName: PropTypes.string,
  style: PropTypes.object,
  headerStyle: PropTypes.object,
  gridStyle: PropTypes.object,
  rowStyle: PropTypes.object,
  onColumnClick: PropTypes.func,
  onHeaderClick: PropTypes.func,
  onRowDoubleClick: PropTypes.func,
  onRowRightClick: PropTypes.func,
  onScroll: PropTypes.func,
  onRowClick: PropTypes.func,
  overscanRowCount: PropTypes.number,
  scrollToIndex: PropTypes.number,
  defaultSortBy: PropTypes.string,
};

TableVirtualized.defaultProps = {
  data: [],
  width: 100,
  height: 400,
  headerHeight: 40,
  rowHeight: 30,
  className: '',
  gridClassName: '',
  rowClassName: '',
  headerClassName: '',
  columnsData: [
    {
      label: 'Header',
      dataKey: 'key',
      columnWidth: 100,
      id: '',
      disableSort: false,
    }
  ],
  style: {},
  headerStyle: {},
  gridStyle: {},
  rowStyle: {},
  disableHeader: false,
  noRowsRenderer: () => <div>No Data</div>,
  onColumnClick: (x) => x,
  onHeaderClick: (x) => x,
  onRowClick: (x) => x,
  onRowDoubleClick: (x) => x,
  onRowRightClick: (x) => x,
  onScroll: (x) => x,
  overscanRowCount: 2,
  scrollToIndex: 0,
  textAlign: 'center',
  defaultSortBy: '',
};
