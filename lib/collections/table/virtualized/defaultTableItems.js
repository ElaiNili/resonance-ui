import React from 'react';

import { Flexbox, Icon } from '../../../index.js';

const defaultCellRenderer = (data) => <div className='table-cell-item'>{data.cellData}</div>;


function defaultHeaderCellRenderer(data) {
  const { dataKey, sortBy, sortDirection, label, disableSort } = data;
  const sortIconRotate = sortDirection === 'ASC' ? '180' : null;
  return (
    <Flexbox center>
      {sortBy !== dataKey
        && <div className='table-header-ellipsis'>{label}</div>}

      {!disableSort && sortBy === dataKey && (
        <Icon icon={'sort'}
          mirrorY={sortDirection === 'ASC'}
          rotate={sortIconRotate}/>
      )}
    </Flexbox>
  );
}


export {
  defaultCellRenderer,
  defaultHeaderCellRenderer,
};

