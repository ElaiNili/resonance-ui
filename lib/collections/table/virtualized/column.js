import React from 'react';

import { Column } from 'react-virtualized';
import { defaultCellRenderer, defaultHeaderCellRenderer } from './defaultTableItems';

import clsx from 'clsx';


export default function Columns(columnsData) {

  const cellRendererWrapper = (cellFn) => {
    if (cellFn) return cellFn;
    else return defaultCellRenderer;
  };

  const cellRenderHeaderWrapper = (cellFn) => {
    if (cellFn) return cellFn;
    else return defaultHeaderCellRenderer;
  };

  const cols = columnsData.map((d, i) => {
    const {
      label,
      dataKey,
      columnWidth,
      cellRenderer,
      headerRenderer,

      id,
      className,
      headerClassName,

      style,
      headerStyle,

      disableSort,
    } = d;

    const defaultCellRenderer = cellRendererWrapper(cellRenderer);

    const defaultHeaderCellRender = cellRenderHeaderWrapper(headerRenderer);

    // ==============
    const defaultHeaderClassName = [
      ''
    ];

    const updatedHeaderClassName =
      clsx(defaultHeaderClassName, headerClassName);

    // ==============

    return (
      <Column
        cellRenderer={defaultCellRenderer}
        className={className}
        dataKey={dataKey}
        disableSort={disableSort}
        headerClassName={updatedHeaderClassName}
        headerRenderer={defaultHeaderCellRender}
        headerStyle={headerStyle}
        id={id}
        key={i}
        label={label}
        style={style}
        width={columnWidth}
        // flexGrow={100} //?
        // flexShrink={1000}


        // maxWidth={}
        // minWidth={}

        // cellDataGetter={(data) => console.log(data)} //?

      // defaultSortDirection={}
      />
    );
  });

  return cols;
}
