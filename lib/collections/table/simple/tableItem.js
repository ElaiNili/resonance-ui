import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';
import types from '../../../types';

function TableItem({
  children,
  className,
  style,
  id,

  as,
  positive,
  negative,
  textAlign,
  verticalAlign,
  colSpan,
  rowSpan,
  color,
}) {
  const defaultClasses = [
    'table-cell-item',
    positive && 'table-positive-cell',
    negative && 'table-negative-cell',
    textAlign && `table-item-text-align-${textAlign}`,
    verticalAlign && `table-item-vertical-align-${verticalAlign}`,
    color && `exchange-item-${color}`,
  ];

  const updatedClasses = clsx(defaultClasses, className);
  const styleDefault = { ...style };

  const Tag = `${as}`;

  return (
    <Tag
      colSpan={colSpan}
      id={id}
      rowSpan={rowSpan}
    >
      <div
        className={updatedClasses}
        style={styleDefault}
      >
        {children}
      </div>
    </Tag>
  );
}

TableItem.propTypes = {
  as: PropTypes.oneOf(['th', 'td']),
  textAlign: PropTypes.oneOf(['center', 'left', 'right']),
  verticalAlign: PropTypes.oneOf(['bottom', 'middle', 'top']),
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  positive: PropTypes.bool,
  negative: PropTypes.bool,
  colSpan: PropTypes.string,
  rowSpan: PropTypes.string,
  color: types.color,
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
};

TableItem.defaultProps = {
  as: 'td',
  children: null,
  className: '',
  style: {},
  // positive: false,
  // negative: false,
  textAlign: 'center',
  verticalAlign: 'middle',
  colSpan: '1',
  rowSpan: '1',
  color: '',
  id: '',
};

export default TableItem;
