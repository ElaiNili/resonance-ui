import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

function TableFooter({ children, className, style, id, }) {
  const defaultClasses = ['simple-table-footer'];

  const updatedClasses = clsx(defaultClasses, className);

  const styleDefault = { ...style };

  return (
    <tfoot className={updatedClasses}
      id={id}
      style={styleDefault}>
      {children}
    </tfoot>
  );
}

TableFooter.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
};

TableFooter.defaultProps = {
  children: null,
  className: '',
  style: {},
  id: '',
};

export default TableFooter;
