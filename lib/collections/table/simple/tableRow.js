import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

function TableRow({ children, className, style, id }) {
  const defaultClasses = [
    'simple-table-row',
  ];

  const updatedClasses = clsx(defaultClasses, className);

  const styleDefault = { ...style };

  return (
    <tr className={updatedClasses}
      id={id}
      style={styleDefault}>
      {children}
    </tr>
  );
}

TableRow.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,

  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
};

TableRow.defaultProps = {
  children: null,
  className: '',
  style: {},
};


export default TableRow;
