import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

function TableBody({ children, className, style, id }) {
  const defaultClasses = ['simple-table-body'];

  const updatedClasses = clsx(defaultClasses, className);

  const styleDefault = { ...style };

  return (
    <tbody className={updatedClasses}
      id={id}
      style={styleDefault}>
      {children}
    </tbody>
  );
}

TableBody.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
};

TableBody.defaultProps = {
  children: null,
  className: '',
  style: {},
  id: '',
};

export default TableBody;
