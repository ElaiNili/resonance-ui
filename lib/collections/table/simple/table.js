import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

export default function Table({
  id,
  children,
  className,
  style,

  basic,
  outlined,
  borderless,
  fluid,
  borderlessWrapper,
}) {
  const defaultClasses = [
    'simple-table',
    !borderless && 'table-border table-border-header',
    !borderless && !outlined && 'table-border-cell',
    !basic && !outlined && !borderless && 'default-table-border',
    // basic && 'table-border-cell',
    // basic && 'table-border table-border-header table-border-bottom',
    // outlined && '',
    borderlessWrapper && 'no-border-wrapper',
    fluid && 'table-fluid',
  ];
  const updatedClasses = clsx(defaultClasses, className);
  const styleDefault = { ...style };

  return (
    <table className={updatedClasses}
      id={id}
      style={styleDefault}>
      {children}
    </table>
  );
}

Table.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  borderless: PropTypes.bool,
  fluid: PropTypes.bool,
  basic: PropTypes.bool,
  outlined: PropTypes.bool,
  borderlessWrapper: PropTypes.bool,
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number, ]),
};

Table.defaultProps = {
  children: null,
  className: '',
  style: {},
  // borderless: false,
  // fluid: false,
  id: '',
};


