import React, { useMemo } from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';

function TableHeader({ children, className, style, id }) {

  const defaultClasses = [
    'simple-table-header',
  ];

  const updatedClasses = clsx(defaultClasses, className);

  const styleDefault = { ...style };


  // const updatedChildren = useMemo(() => {
  //   if (children) {
  //     if (Array.isArray(children) === false) {
  //       const row = children.props;
  //       if (row.children) {
  //         if (Array.isArray(row.children)) {
  //           const updChildren = {};
  //           for (const key in children) {
  //             if (Object.hasOwnProperty.call(children, key)) {
  //               const item = children[key];
  //               updChildren[key] = item;
  //               if (key === 'props') {
  //                 const newChildrenProps = {};
  //                 for (const nestedKey in updChildren[key]) {
  //                   // eslint-disable-next-line max-len
  //                   if (Object.hasOwnProperty.call(updChildren[key], nestedKey)) {
  //                     const nestedItem = updChildren[key][nestedKey];
  //                     newChildrenProps[nestedKey] = nestedItem;
  //                     if (nestedKey === 'children') {
  //                       newChildrenProps[nestedKey] = row.children.map((d) => ({
  //                         ...d, props: { ...d.props, as: 'th' }
  //                       }));
  //                     }
  //                   }
  //                 }
  //                 updChildren[key] = newChildrenProps;
  //               }
  //             }
  //           }
  //           return updChildren;
  //         } else {
  //           // const newChildren = {};
  //           // for (const key in children) {
  //           //   if (Object.hasOwnProperty.call(children, key)) {
  //           //     const item = children[key];
  //           //     newChildren[key] = item;
  //           //     if (key === 'props') {
  //           //       const newChildrenProps = {};
  //           //       for (const nestedKey in newChildren[key]) {
  //           //         if (Object.hasOwnProperty.call(newChildren[key], nestedKey)) {
  //           //           const nestedItem = newChildren[key][nestedKey];
  //           //           newChildrenProps[nestedKey] = nestedItem;
  //           //           if (nestedKey === 'children') {
  //           //             const newPropChildren = {}
  //           //             console.log(nestedKey, nestedItem);
  //           //             // newChildrenProps[nestedKey] = row.children.map((d) => ({
  //           //             //   ...d, props: { ...d.props, as: 'th' }
  //           //             // }));
  //           //           }
  //           //         }
  //           //       }
  //           //       newChildren[key] = newChildrenProps;
  //           //     }
  //           //   }
  //           // }
  //           // console.log(newChildren);
  //           return children;
  //         }
  //       }
  //     } else {
  //       return children;
  //     }
  //   }
  // }, [children]);

  return (
    <thead className={updatedClasses}
      id={id}
      style={styleDefault}>{children}</thead>
  );
}

TableHeader.propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
};

TableHeader.defaultProps = {
  id: '',
  children: null,
  className: '',
  style: {},
};

export default TableHeader;
