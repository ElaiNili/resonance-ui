import { Table, TableRow, TableItem, TableBody, TableHeader, TableFooter } from './simple/index.js';
import TableVirtualized from './virtualized/index.js';

export {
  Table, TableRow, TableItem, TableBody, TableHeader, TableFooter,
  TableVirtualized
};
