import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';

import Flexbox from '../../elements/flexbox/flexbox';
import { ProgressBar } from '../../modules/progressBar/index';
import clsx from 'clsx';

function ProgressGroupJsx({
  // cur, previous,
  mount,
  filled,
  className,
  style,
  data,
}) {

  // const [state, setState] = useState({ currentItem: cur, previous, });

  const styleWidth = { width: `calc((100% - 2*${mount}px + 2px) / ${mount})` };
  const defaultStyles = { ...styleWidth, ...style, };

  // ================================================================================

  const defaulrClasses = [
    'progress-bar-items'
  ];

  const updatedClasses = clsx(defaulrClasses, className);
  // ================================================================================

  const currentItem = filled - 1;
  // ================================================================================

  if (data.length !== 0) {
    return (
      <Flexbox center>
        {data.map((bar, i) => (
          <ProgressBar
            active={currentItem > i}
            animation={
            // previous < currentItem &&
              currentItem === i}
            className={updatedClasses}
            delay={currentItem > 0 ? 100 : 500}
            empty={currentItem < i}
            key={i}
            style={defaultStyles}
            {...bar}
          />
        ))}
      </Flexbox>
    );
  }

  const barItems = [];


  for (let i = 0; i < mount; i++) {
    barItems.push(
      <ProgressBar
        active={currentItem > i}
        animation={
          // previous < currentItem &&
          currentItem === i}
        className={updatedClasses}
        delay={currentItem > 0 ? 100 : 500}
        empty={currentItem < i}
        key={i}
        style={defaultStyles}
      />
    );
  }

  // const barItems = mount.map((d) => (
  //   <ProgressBar
  //     key={d}
  //   />
  // ));

  return (
    <Flexbox center>
      {barItems}
    </Flexbox>
  );
}

export default function ProgressGroup({
  mount,
  data,
  filled,
  className,
  style,
}) {

  // const [previous, setPrevious] = useState(filled - 1);

  // const cur = mount - 1;

  // console.log('previous => ', previous);
  // console.log('filled => ', filled);

  // useEffect(() => {
  //   setPrevious((s) => {
  //     if (s === filled - 1) return s;
  //     if (s !== filled - 1) return filled - 1;
  //   });
  // }, [filled]);




  return (
    <ProgressGroupJsx
      // cur={cur}
      className={className}
      data={data}
      filled={filled}
      mount={mount}
      style={style}
      // previous={previous}
    />
  );

}

ProgressGroup.propTypes = {
  mount: PropTypes.number,
  filled: PropTypes.number,
  className: PropTypes.string,
  style: PropTypes.object,
  data: PropTypes.array,
};

ProgressGroup.defaultProps = {
  mount: 4,
  filled: 1,
  className: '',
  style: {},
  data: [],
};
