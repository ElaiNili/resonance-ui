import React from 'react';

import PropTypes from 'prop-types';


export default function CellRenderer(
  { columnIndex, key, rowIndex, style, list, className, }) {

  return (
    <div className={className}
      key={key}
      style={style}>
      {list[rowIndex][columnIndex]}
    </div>
  );
}




CellRenderer.propTypes = {
  columnIndex: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  key: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  rowIndex: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  style: PropTypes.object,
  list: PropTypes.array,
  className: PropTypes.string,
};

CellRenderer.defaultProps = {
  style: {},
  className: '',
};
