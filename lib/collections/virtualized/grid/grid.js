import React from 'react';

import PropTypes from 'prop-types';

import { Grid } from 'react-virtualized';
import clsx from 'clsx';

export default function GridVirtualized({
  cellRenderer, columnWidth,
  height, rowHeight, width,
  columnCount, rowCount, className, style, containerStyle, id,
}) {

  const defaultClasses = [
    'grid-virtualized'
  ];

  const updatedClasses = clsx(defaultClasses, className);

  return (
    <Grid
      cellRenderer={cellRenderer}
      className={updatedClasses}
      columnCount={columnCount}
      columnWidth={columnWidth}
      containerStyle={containerStyle}
      height={height}
      id={id}
      rowCount={rowCount}
      rowHeight={rowHeight}
      style={style}
      width={width}
    // autoContainerWidth={bool}
    // autoHeight={bool}
    // autoWidth={bool}
    // cellRangeRenderer={fn}
    // containerProps={obj}
    // containerRole={str}
    // deferredMeasurementCache
    // estimatedColumnSize={num}
    // estimatedRowSize={num}
    // isScrolling={bool}
    // isScrollingOptOut={bool}
    // noContentRenderer={fn}
    // onSectionRendered={fn}
    // onScroll={fn}
    // onScrollbarPresenceChange={fn}
    // overscanColumnCount={num}
    // overscanIndicesGetter={fn}
    // overscanRowCount={num}
    // role={str}
    // scrollingResetTimeInterval={300}
    // scrollLeft={num}
    // scrollToAlignment={str}
    //* scrollToColumn={num}
    //* scrollToRow={num}
    //*  scrollTop={1}
    // tabIndex={1}
    />
  );
}


GridVirtualized.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  cellRenderer: PropTypes.func.isRequired,
  columnCount: PropTypes.number.isRequired,

  columnWidth: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  height: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  rowCount: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  rowHeight: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  width: PropTypes.number.isRequired,
  className: PropTypes.string,
  style: PropTypes.object,
  containerStyle: PropTypes.object,

};

GridVirtualized.defaultProps = {
  className: '',
  style: {},
  containerStyle: {},
};
