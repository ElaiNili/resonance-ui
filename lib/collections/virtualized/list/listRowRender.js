import React from 'react';

import PropTypes from 'prop-types';

export default function RowRenderer({
  style,
  className,
  children,
}) {

  return (
    <div className={className}
      style={style}>
      {children}
    </div>
  );
}

RowRenderer.propTypes = {
  style: PropTypes.object,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
};

RowRenderer.defaultProps = {
  style: {},
  className: '',
  children: '',

};
