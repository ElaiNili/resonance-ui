import React from 'react';

import PropTypes from 'prop-types';

import List from 'react-virtualized/dist/commonjs/List';
import clsx from 'clsx';

export default function ListVirtualized({
  width,
  height,
  rowCount,
  rowHeight,
  rowRenderer,
  // data,
  className,
  style,
  id,
}) {

  const defaultClasses = [
    'list-virtualized'
  ];

  const updatedClasses = clsx(defaultClasses, className);

  return (
    <List
      className={updatedClasses}
      height={height}
      id={id}
      rowCount={rowCount}
      rowHeight={rowHeight}
      rowRenderer={rowRenderer}
      style={style}
      width={width}
      // estimatedRowSize
      // noRowsRenderer
      // onRowsRendered
      // onScroll
      // scrollToAlignment
      // scrollToIndex
      // scrollTop
      // tabIndex
    />
  );
}

ListVirtualized.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  rowCount: PropTypes.number,
  rowRenderer: PropTypes.func,
  rowHeight: PropTypes.number,
  className: PropTypes.string,
  style: PropTypes.object,
  id: PropTypes.string,
  // data: PropTypes.array,
};

ListVirtualized.defaultProps = {
  className: '',
  style: {},
  id: '',
  // data: [],
  width: 100,
  height: 100,
  rowCount: 100,
  rowHeight: 20,
  rowRenderer: (renderProps) =>
    <div {...renderProps}>{[][renderProps.index]}</div>,

};
