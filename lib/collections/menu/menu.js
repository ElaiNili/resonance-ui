import React from 'react';

import clsx from 'clsx';
import PropTypes from 'prop-types';


export default function Menu({
  id,
  children,
  style,
  className,
  rows,
  vertical,
}) {
  const gridColumns = typeof rows !== 'string' ? `repeat(${rows}, 1fr)` : rows;

  const defaultStyle = {
    ...style,
  };

  if (!vertical) defaultStyle.gridTemplateColumns = gridColumns;

  const defaultClasses = [
    'menu',
    vertical && 'menu-vertical'
  ];
  const updatedClass = clsx(defaultClasses, className);

  return (
    <div className={updatedClass}
      id={id}
      style={defaultStyle}>
      {children}
    </div>
  );
}


Menu.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  style: PropTypes.object,
  className: PropTypes.string,
  rows: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  vertical: PropTypes.bool,
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
};

Menu.defaultProps = {
  children: null,
  style: {},
  className: '',
  rows: 3,
  id: '',
};
