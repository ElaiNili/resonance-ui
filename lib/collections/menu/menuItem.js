import React from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';


export default function MenuItem({
  id,
  children,
  className,
  style,
  align,
}) {
  const defaultClasses = [
    'menu-item',
    align && `menu-item-align-${align}`,
  ];
  const updatedClass = clsx(defaultClasses, className);

  return (
    <div className={updatedClass}
      id={id}
      style={style}>
      {children}
    </div>
  );
}

MenuItem.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  align: PropTypes.oneOf([ 'start', 'center', 'end', ]),
};

MenuItem.defaultProps = {
  children: null,
  className: '',
  style: {},
};

