import React from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';


export default function LeftMenuItems({
  children,
  className,
  style,
  id,

  center,
  inline,
  left,
  right,
}) {
  const defaultClasses = [
    'left-menu-item',
    center && 'left-menu-item-center-flex',
    inline && 'left-menu-item-row',
    left && 'left-menu-item-flex-left',
    right && 'left-menu-item-flex-right',
  ];

  const updatedClass = clsx(defaultClasses, className);

  return (
    <div className={updatedClass}
      id={id}
      style={style}>{children}</div>
  );
}

LeftMenuItems.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  center: PropTypes.bool,
  inline: PropTypes.bool,
  left: PropTypes.bool,
  right: PropTypes.bool,
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
};

LeftMenuItems.defaultProps = {
  children: null,
  className: '',
  style: {},
  center: false,
  inline: false,
  left: false,
  right: false,
  id: '',
};
