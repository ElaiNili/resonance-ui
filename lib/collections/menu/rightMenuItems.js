import React from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';


export default function RightMenuItems({
  children,
  className,
  style,
  id,

  center,
  inline,
  left,
  right,
}) {
  const defaultClasses = [
    'right-menu-item',
    center && 'right-menu-item-center-flex',
    inline && 'right-menu-item-row',
    left && 'right-menu-item-flex-left',
    right && 'right-menu-item-flex-right',
  ];
  const updatedClass = clsx(defaultClasses, className);

  return (
    <div className={updatedClass}
      id={id}
      style={style}>{children}</div>
  );
}

RightMenuItems.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  center: PropTypes.bool,
  inline: PropTypes.bool,
  left: PropTypes.bool,
  right: PropTypes.bool,
  className: PropTypes.string,
  style: PropTypes.object,
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
};

RightMenuItems.defaultProps = {
  children: null,
  // center: false,
  // inline: false,
  // left: false,
  // right: false,
  className: '',
  style: {},
  id: '',
};

