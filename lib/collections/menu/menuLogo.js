import React from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';


export default function MenuLogo({ img, className, alt, style, id }) {

  const defaultClasses = [
    'menu-logo',
    'flexbox flexbox-center flexbox-content-center',
    'pointer-cursor'
  ];

  const updatedClass = clsx(defaultClasses, className);

  const onClick = () => window.open('/', '_blank');

  return (
    <div className={updatedClass}
      id={id}
      onClick={onClick}
      style={style}
    >
      <img alt={alt}
        src={img} />
    </div>
  );
}

MenuLogo.propTypes = {
  img: PropTypes.string,
  className: PropTypes.string,
  alt: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  style: PropTypes.object,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

MenuLogo.defaultProps = {
  className: '',
  img: '',
  alt: '',
  style: {},
  id: '',
};


