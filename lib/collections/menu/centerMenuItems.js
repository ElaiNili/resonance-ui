import React from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';


export default function CenterMenuItems(
  { children, center, className, style, id, }
) {
  const defaultClasses = [
    'center-menu-item',
    center && 'center-menu-item-flex',
  ];
  const updatedClass = clsx(defaultClasses, className);

  return (
    <div className={updatedClass}
      id={id}
      style={style}>{children}</div>
  );
}

CenterMenuItems.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  center: PropTypes.bool,
  className: PropTypes.string,
  style: PropTypes.object,
  id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
};

CenterMenuItems.defaultProps = {
  children: null,
  center: false,
  className: '',
  style: {},
  id: '',

};
