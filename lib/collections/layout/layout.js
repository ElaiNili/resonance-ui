import React, { forwardRef } from 'react';

import PropTypes from 'prop-types';
import clsx from 'clsx';

import RGL, { WidthProvider } from 'react-grid-layout';


const ReactGridLayout = WidthProvider(RGL);

function LayoutHeader({ children, className, style }) {

  const defaultClasses = [
    'drag-item',
    'layout-header',
  ];

  const updatedClass = clsx(defaultClasses, className);

  return (
    <div className={updatedClass}
      style={style} >
      {children}
    </div>
  );
}

LayoutHeader.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
};

LayoutHeader.defaultProps = {
  children: '',
  className: '',
  style: {},
};



function LayoutContent({ children, className, style, id }) {

  const defaultClasses = [
    'non-drag',
    'layout-content',
  ];

  const updatedClass = clsx(defaultClasses, className);

  return (
    <div className={updatedClass}
      id={id}
      style={style} >
      {children}
    </div>
  );
}

LayoutContent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: PropTypes.string,
  style: PropTypes.object,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

LayoutContent.defaultProps = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  className: '',
  style: {},
  id: '',
};

// function LayoutItem({ }) {
//     return forwardRef((elem, ref) => {

//         const { style, className, children, ...props } = elem;

//         return <div
//             style={style}
//             className={className}
//             ref={ref}
//             {...props}
//         >
//             {children}
//         </div>;
//     });
// };

const LayoutItem = forwardRef((elem, ref) => {
  const { style, className, children, ...props } = elem;
  return (
    <div
      className={className}
      ref={ref}
      style={style}
      {...props}
    >
      {children}
    </div>
  );
});

LayoutItem.displayName = 'LayoutItem';

LayoutItem.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
    PropTypes.func,
  ]),
  style: PropTypes.object,
  className: PropTypes.string,
  'data-grid': PropTypes.shape({
    i: PropTypes.string,
    x: PropTypes.number,
    y: PropTypes.number,
    w: PropTypes.number,
    h: PropTypes.number,
    minW: PropTypes.number,
    maxW: PropTypes.number,
    minH: PropTypes.number,
    maxH: PropTypes.number,
    static: PropTypes.bool,
    isDraggable: PropTypes.bool,
    isResizable: PropTypes.bool,
    resizeHandles: PropTypes.arrayOf(PropTypes.string),
    isBounded: PropTypes.bool,
  }).isRequired,
};

function Layout({
  children,
  className,
  layout, cols, rowHeight, width, margin,
  containerPadding,
  // draggableCancel, draggableHandle,
  resizeHandles,
  allowOverlap, //
  compactType, //
  onLayoutChange,
  onDragStart, onDrag, onDragStop,
  onResizeStart, onResize, onResizeStop,
  onBreakpointChange, onWidthChange,
  ready, //
}) {

  return (
    <ReactGridLayout
      allowOverlap={allowOverlap}
      breakpoints={{ lg: 1200, md: 996, sm: 768, xs: 480, xxs: 0 }}
      className={className}
      cols={cols}
      compactType={compactType}
      containerPadding={containerPadding}
      draggableCancel={'.non-drag'}
      draggableHandle={'.drag-item'}
      layout={layout}
      margin={margin}
      onBreakpointChange={onBreakpointChange}
      onDrag={onDrag}
      onDragStart={onDragStart}
      onDragStop={onDragStop}
      onLayoutChange={onLayoutChange}
      onResize={onResize}
      onResizeStart={onResizeStart}
      onResizeStop={onResizeStop}
      onWidthChange={onWidthChange}
      resizeHandles={resizeHandles}
      rowHeight={rowHeight}
      width={width}
    >
      {ready && children}
    </ReactGridLayout>
  );
}

Layout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
    PropTypes.object,
    PropTypes.array,
  ]),
  ready: PropTypes.bool,
  cols: PropTypes.number,
  rowHeight: PropTypes.number,
  width: PropTypes.number,
  margin: PropTypes.arrayOf(PropTypes.number),
  containerPadding: PropTypes.arrayOf(PropTypes.number),
  compactType: PropTypes.oneOf(['vertical', 'horizontal']),
  allowOverlap: PropTypes.bool,
  draggableCancel: PropTypes.string,
  draggableHandle: PropTypes.string,
  resizeHandles: PropTypes.arrayOf(PropTypes.string),
  onLayoutChange: PropTypes.func,
  onDragStart: PropTypes.func,
  onDrag: PropTypes.func,
  onDragStop: PropTypes.func,
  onResizeStart: PropTypes.func,
  onResize: PropTypes.func,
  onResizeStop: PropTypes.func,
  breakpoints: PropTypes.object,
  onBreakpointChange: PropTypes.func,
  onWidthChange: PropTypes.func,
  className: PropTypes.string,
  layout: PropTypes.string
};

Layout.defaultProps = {
  children: undefined,
  ready: false,
  cols: 10,
  rowHeight: 30,
  width: 200,
  margin: [3, 3],
  containerPadding: [5, 5],
  draggableCancel: '',
  draggableHandle: '',
  resizeHandles: ['s', 'e', 'se'], // 's', 'w', 'e', 'n', 'sw', 'nw', 'se', 'ne'
  onLayoutChange: (x) => x,
  onDragStart: (x) => x,
  onDrag: (x) => x,
  onDragStop: (x) => x,
  onResizeStart: (x) => x,
  onResize: (x) => x,
  onResizeStop: (x) => x,
  breakpoints: {},
  onBreakpointChange: (x) => x,
  onWidthChange: (x) => x,
  className: '',
};

export { Layout, LayoutItem, LayoutHeader, LayoutContent };
