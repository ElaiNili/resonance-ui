build:	
		export BUILD=true && npm run clean && npm run build

build_dock:	
		export BUILD=false && export PUBLIC_PATH=/rui && rm -rf node_modules && rm package-lock.json && npm i && npm run clean_dock && npm run build

build_clean:	
		export BUILD=true && npm run build

dev:	
		export BUILD=false && npm run dev

remove_nm:
		rm -rf node_modules

npm_i:	
		npm i

clean:
		rm -rf node_modules && rm package-lock.json && npm i
