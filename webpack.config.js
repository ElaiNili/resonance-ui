const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const isBuild = JSON.parse(process.env.BUILD);
const publicPath = process.env.PUBLIC_PATH || '/';
const MODE = isBuild ? 'production' : 'development';

const config = {
  mode: 'development',
  entry: './lib/index.js',
  devtool: 'inline-source-map',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.js',
    library: 'resonance-ui',
    libraryTarget: 'umd',
    umdNamedDefine: true,
    clean: true,
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'style.css'
    })
  ],
  module: {
    rules: [
      {
        test: /\.js$|jsx/,
        exclude: /node_modules/,
        include: path.resolve(__dirname, 'lib'),
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', ['@babel/preset-react', { 'runtime': 'automatic' }]],
            cacheDirectory: true
          }
        }
      },
      {
        test: /\.css$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader'
        ],
      },
      {
        test: /\.(png|jp(e*)g|svg|gif)$/,
        use: ['file-loader'],
      },
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  externals: {
    react: 'react'
  }
};

const docs = {
  mode: MODE,
  entry: path.join(__dirname, 'docs', 'index.js'),
  output: {
    path: path.resolve(__dirname, 'rui'),
    publicPath,
  },
  module: {
    rules: [
      {
        test: /\.js$|jsx/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jp(e*)g|svg|gif)$/,
        use: ['file-loader'],
      },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'docs', 'index.html'),
      favicon: path.resolve(__dirname, 'docs', 'favicon.png')
    }),
  ],
};

// eslint-disable-next-line no-unused-vars
module.exports = (env, argv) => {
  if (isBuild) return config;
  else return docs;
};
