# Resonance UI 
#### (Resonance styled components)

## How update Lib:

- write code
- export your code to lib/index.js
- change version in package.json (1.2.3 where 1-major, 2-minor, 3-fix)
- in console write: make build
- make git commit and push

## How update docs

- create components in docs/components/elements
- add route in docs/components/routes
- start app: make dev

